package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"runtime/pprof"

	"github.com/EdwardDowling/ray-tracer/lib"
)

var profile = flag.Bool("profile", false, "write profile to file")
var iterCount = flag.Int("count", 1, "iteration cound")

func main() {
	flag.Parse()
	if *profile {
		f, err := os.Create("cpuprofile.prof")
		if err != nil {
			log.Fatal(err)
		}
		defer f.Close()
		memf, err := os.Create("memprofile.prof")
		if err != nil {
			log.Fatal(err)
		}
		defer memf.Close()
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
		defer pprof.WriteHeapProfile(memf)
	}

	max := *iterCount
	for i := 0; i < *iterCount; i++ {
		fileName := fmt.Sprintf("/tmp/temp-%04d.ppm", i)
		fmt.Println(i+1, "/", max)
		f, err := os.Create(fileName)
		if err != nil {
			fmt.Println("someErr", err)
		}
		bw := bufio.NewWriter(f)

		bw = bufio.NewWriterSize(bw, 68000000)
		lib.Test(bw, i, max)
		bw.Flush()

		f.Close()
	}

}
