package lib

import (
	"math"
)

type cone struct {
	min      float64
	max      float64
	isClosed bool
}

func newCone(min, max float64) *object {
	o := newObject()
	o.bo = &cone{
		min: min,
		max: max,
	}
	return o
}

func newClosedCone(min, max float64) *object {
	o := newObject()
	o.bo = &cone{
		min:      min,
		max:      max,
		isClosed: true,
	}
	return o
}

func (con *cone) sdf(p *tuple) float64 {
	panic(1)
}

func (con *cone) localIntersects(ob *object, r *ray) []*intersection {
	xs := []*intersection{}
	a := r.direction.x*r.direction.x -
		r.direction.y*r.direction.y +
		r.direction.z*r.direction.z
	b := 2*r.origin.x*r.direction.x -
		2*r.origin.y*r.direction.y +
		2*r.origin.z*r.direction.z
	c := r.origin.x*r.origin.x -
		r.origin.y*r.origin.y +
		r.origin.z*r.origin.z

	if isClose(a, EPSILON) {
		if !isClose(b, EPSILON) {
			xs = []*intersection{
				newIntersection(-c/2*b, ob),
			}
			return con.intersectsCaps(r, ob, xs)
		}
	}
	disc := b*b - 4*a*c
	if disc < 0.0 {
		return []*intersection{}
	}
	t0 := (-b - math.Sqrt(disc)) / (2 * a)
	t1 := (-b + math.Sqrt(disc)) / (2 * a)

	// if t0 > t1 {
	// 	t0, t1 = t1, t0
	// }

	y0 := r.origin.y + t0*r.direction.y
	if con.min < y0 && y0 < con.max {
		xs = insertIntersection(newIntersection(t0, ob), xs)
	}
	y1 := r.origin.y + t1*r.direction.y
	if con.min < y1 && y1 < con.max {
		xs = insertIntersection(newIntersection(t1, ob), xs)
	}
	return con.intersectsCaps(r, ob, xs)
}

func (c *cone) checkCap(y float64, t float64, r *ray) bool {
	x := r.origin.x + t*r.direction.x
	z := r.origin.z + t*r.direction.z
	return (x*x + z*z) <= y*y
}

func (c *cone) intersectsCaps(r *ray, ob *object, xs []*intersection) []*intersection {
	if !c.isClosed || isClose(r.direction.y, EPSILON) {
		return xs
	}
	t := (c.min - r.origin.y) / r.direction.y
	if c.checkCap(c.min, t, r) {
		xs = insertIntersection(newIntersection(t, ob), xs)
	}
	t = (c.max - r.origin.y) / r.direction.y
	if c.checkCap(c.max, t, r) {
		xs = insertIntersection(newIntersection(t, ob), xs)
	}
	return xs
}

func (c *cone) localNormalAt(p *tuple, _ *intersection) *tuple {
	dist := p.x*p.x + p.z*p.z
	if dist < 1.0 && p.y >= (c.max-EPSILON) {
		return newVector(0, 1, 0)
	}
	if dist < 1.0 && p.y <= (c.min+EPSILON) {
		return newVector(0, -1, 0)
	}
	y := math.Sqrt(dist) + EPSILON
	if y > 0 {
		y = -y
	}
	return newVector(p.x, y, p.z).normalize()
}

func (c *cone) equals(other baseObject) bool {
	_, ok := other.(*cone)
	return ok
}

func (c *cone) bounds() (*tuple, *tuple) {
	minY := -INFINITY
	maxY := INFINITY
	if c.isClosed {
		minY = c.min
		maxY = c.max
	}
	min := newPoint(minY, minY, minY)
	max := newPoint(maxY, maxY, maxY)
	return min, max
}

func (c *cone) getLines() []*line {
	return nil
}
