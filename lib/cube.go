package lib

import "math"

type cube struct{}

func newCube() *object {
	o := newObject()
	o.bo = &cube{}
	return o
}

func (c *cube) localIntersects(ob *object, r *ray) []*intersection {
	xTMin, xTMax := c.checkAxis(r.origin.x, r.direction.x)
	yTMin, yTMax := c.checkAxis(r.origin.y, r.direction.y)
	zTMin, zTMax := c.checkAxis(r.origin.z, r.direction.z)

	tMin := math.Max(xTMin, math.Max(yTMin, zTMin))
	tMax := math.Min(xTMax, math.Min(yTMax, zTMax))

	if tMin > tMax || tMax < 0.0 {
		return []*intersection{}
	}
	return []*intersection{
		newIntersection(tMin, ob),
		newIntersection(tMax, ob),
	}
}

func (c *cube) sdf(p *tuple) float64 {
	// vec3 q = abs(p) - b;
	// return length(max(q,0.0)) + min(max(q.x,max(q.y,q.z)),0.0);
	q := newVector(math.Abs(p.x), math.Abs(p.y), math.Abs(p.z)).
		sub(newPoint(1, 1, 1))

	return newPoint(math.Max(q.x, 0.0),
		math.Max(q.y, 0.0),
		math.Max(q.z, 0.0)).mag() -
		math.Min(math.Max(q.x, math.Max(q.y, q.z)), 0.0)
}

func (c *cube) checkAxis(origin, direction float64) (float64, float64) {
	// Offset plane intersection
	tMinNumerator := (-1.0 - origin)
	tMaxNumerator := (1.0 - origin)
	var tMin float64
	var tMax float64
	if math.Abs(direction) >= EPSILON {
		tMin = tMinNumerator / direction
		tMax = tMaxNumerator / direction
	} else {
		tMin = tMinNumerator * INFINITY
		tMax = tMaxNumerator * INFINITY
	}
	if tMin > tMax {
		tMin, tMax = tMax, tMin
	}
	return tMin, tMax
}

func (c *cube) localNormalAt(p *tuple, _ *intersection) *tuple {
	maxC := math.Max(math.Abs(p.x),
		math.Max(math.Abs(p.y), math.Abs(p.z)))
	if isClose(maxC, math.Abs(p.x)) {
		return newVector(p.x, 0, 0)
	} else if isClose(maxC, math.Abs(p.y)) {
		return newVector(0, p.y, 0)
	}
	return newVector(0, 0, p.z)
}

func (c *cube) equals(other baseObject) bool {
	_, ok := other.(*cube)
	return ok
}

func (c *cube) bounds() (*tuple, *tuple) {
	return newPoint(-1, -1, -1), newPoint(1, 1, 1)
}

func (_ *cube) getLines() []*line {
	a := newPoint(-1, -1, -1)
	b := newPoint(1, -1, -1)
	c := newPoint(1, -1, 1)
	d := newPoint(-1, -1, 1)

	e := newPoint(-1, 1, -1)
	f := newPoint(1, 1, -1)
	g := newPoint(1, 1, 1)
	h := newPoint(-1, 1, 1)
	return []*line{
		{p1: a, p2: b},
		{p1: b, p2: c},
		{p1: c, p2: d},
		{p1: d, p2: a},

		{p1: a, p2: e},
		{p1: b, p2: f},
		{p1: c, p2: g},
		{p1: d, p2: h},

		{p1: e, p2: f},
		{p1: f, p2: g},
		{p1: g, p2: h},
		{p1: h, p2: e},
	}
}
