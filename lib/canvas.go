package lib

import (
	"fmt"
	"io"
)

type canvas struct {
	width  int
	height int
	pixels [][]*color
	depth  [][]float64
}

func newCanvas(width, height int) *canvas {
	c := canvas{
		width:  width,
		height: height,
	}

	c.pixels = make([][]*color, width)

	for rowIndex := range c.pixels {
		c.pixels[rowIndex] = make([]*color, height)
		for pixelIndex := range c.pixels[rowIndex] {
			c.pixels[rowIndex][pixelIndex] = newColor(0, 0, 0)
		}
	}
	return &c
}

func (c *canvas) put(x, y int, col *color) {
	c.pixels[x][y] = col
}

func (c *canvas) get(x, y int) *color {
	return c.pixels[x][y]
}

func (c *canvas) toPPM(w io.Writer) error {
	if _, err := w.Write([]byte("P3\n")); err != nil {
		return err
	}

	if _, err := fmt.Fprintf(w, "%d %d\n", c.width, c.height); err != nil {
		return err
	}
	if _, err := w.Write([]byte("255\n")); err != nil {
		return err
	}
	for y := 0; y < c.height; y++ {
		currentLength := 0
		for x := 0; x < c.width; x++ {
			pixel := c.get(x, y)
			for _, s := range pixel.toStrings() {
				finalS := s
				if currentLength != 0 {
					finalS = " " + finalS
				}
				if currentLength+len(finalS) > 70 {
					if _, err := w.Write([]byte("\n" + s)); err != nil {
						return err
					}
					currentLength = len("\n" + s)
					continue
				}
				currentLength += len(finalS)
				if _, err := w.Write([]byte(finalS)); err != nil {
					return err
				}
			}
		}
		if _, err := w.Write([]byte("\n")); err != nil {
			return err
		}
		currentLength = 0
	}
	return nil
}

func (c *canvas) drawLine(x0, y0, x1, y1 int, col *color) {
	dx := x1 - x0
	dy := y1 - y0
	if dx < 0 {
		dx = -dx
	}
	if dy < 0 {
		dy = -dy
	}
	if dy < dx {
		if x0 > x1 {
			c.drawLineLow(x1, y1, x0, y0, col)
			return
		}
		c.drawLineLow(x0, y0, x1, y1, col)
		return
	}
	if y0 > y1 {
		c.drawLineHigh(x1, y1, x0, y0, col)
		return
	}
	c.drawLineHigh(x0, y0, x1, y1, col)
}

func (c *canvas) drawLineLow(x0, y0, x1, y1 int, col *color) {
	dx := x1 - x0
	dy := y1 - y0
	yi := 1
	if dy < 0 {
		yi = -1.0
		dy = -dy
	}
	d := 2*dy - dx
	y := y0
	for x := x0; x <= x1; x++ {
		if x < c.width && y < c.height &&
			x >= 0 && y >= 0 {
			c.put(x, y, col)
		}
		if d > 0 {
			y += yi
			d += 2 * (dy - dx)
		} else {
			d += 2 * dy
		}
	}
}

func (c *canvas) drawLineHigh(x0, y0, x1, y1 int, col *color) {
	dx := x1 - x0
	dy := y1 - y0
	xi := 1
	if dx < 0 {
		xi = -1
		dx = -dx
	}
	d := 2*dx - dy
	x := x0
	for y := y0; y <= y1; y++ {
		if x < c.width && y < c.height &&
			x >= 0 && y >= 0 {
			c.put(x, y, col)
		}
		if d > 0 {
			x += xi
			d += 2 * (dx - dy)
		} else {
			d += 2 * dx
		}
	}
}
