package lib

import "testing"

func TestHit(t *testing.T) {
	t.Run("positive t", func(t *testing.T) {
		s := newSphere()
		i1 := newIntersection(1, s)
		i2 := newIntersection(2, s)
		xs := []*intersection{i1, i2}
		h := hit(xs)
		if !h.equals(i1) {
			t.Error("incorrect intersection returned from hit")
		}
	})
	t.Run("negative t", func(t *testing.T) {
		s := newSphere()
		i1 := newIntersection(-1, s)
		i2 := newIntersection(1, s)
		xs := []*intersection{i1, i2}
		h := hit(xs)
		if !h.equals(i2) {
			t.Error("incorrect intersection returned from hit")
		}
	})
	t.Run("all negative t", func(t *testing.T) {
		s := newSphere()
		i1 := newIntersection(-1, s)
		i2 := newIntersection(-2, s)
		xs := []*intersection{i1, i2}
		h := hit(xs)
		if h != nil {
			t.Error("incorrect intersection returned from hit")
		}
	})
	t.Run("many intersections", func(t *testing.T) {
		s := newSphere()
		i1 := newIntersection(5, s)
		i2 := newIntersection(7, s)
		i3 := newIntersection(-3, s)
		i4 := newIntersection(2, s)
		xs := []*intersection{i1, i2, i3, i4}
		h := hit(xs)
		if !h.equals(i4) {
			t.Error("incorrect intersection returned from hit")
		}
	})
}

func TestTransform(t *testing.T) {
	t.Run("translation", func(t *testing.T) {
		r := newRay(newPoint(1, 2, 3), newVector(0, 1, 0))
		m := translation(3, 4, 5)
		if !r.transform(m).equals(newRay(
			newPoint(4, 6, 8), newVector(0, 1, 0))) {
			t.Error("incorrect result of translation")
		}
	})
	t.Run("scaling", func(t *testing.T) {
		r := newRay(newPoint(1, 2, 3), newVector(0, 1, 0))
		m := scaling(2, 3, 4)
		if !r.transform(m).equals(newRay(
			newPoint(2, 6, 12), newVector(0, 3, 0))) {
			t.Error("incorrect result of scaling")
		}
	})
}
