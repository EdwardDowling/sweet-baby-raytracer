package lib

import (
	"math"
)

type SDFWRapperFunc = func(distFunc func(p *tuple) float64, p *tuple) float64
type SDFDistFunc = func(p *tuple) float64

type sdf struct {
	distFunc SDFDistFunc
	wrapper  SDFWRapperFunc
}

func newSDF(distFunc SDFDistFunc) *object {
	o := newObject()
	o.bo = &sdf{
		distFunc: distFunc,
	}
	return o
}

func newSDFWithWrapper(distFunc SDFDistFunc, wrapperFunc SDFWRapperFunc) *object {
	o := newObject()
	o.bo = &sdf{
		distFunc: distFunc,
		wrapper:  wrapperFunc,
	}
	return o
}

func (s *sdf) localIntersects(ob *object, r *ray) []*intersection {
	panic(1)
}

func (s *sdf) sdf(p *tuple) float64 {
	if s.wrapper == nil {
		return s.distFunc(p)
	}
	return s.wrapper(s.distFunc, p)
}

func (s *sdf) localNormalAt(p *tuple, _ *intersection) *tuple {
	panic(1)
}

func (s *sdf) equals(other baseObject) bool {
	_, ok := other.(*sdf)
	return ok
}

func (s *sdf) bounds() (*tuple, *tuple) {
	return newPoint(-1, -1, -1), newPoint(1, 1, 1)
}

func (s *sdf) getLines() []*line {
	return nil
}

func torusSDF(p *tuple) float64 {
	q := newVector(math.Sqrt(p.x*p.x+p.z*p.z)-1, p.y, 0.0)
	return q.mag() - 1
}

func cubeSDF(p *tuple) float64 {
	q := newVector(math.Abs(p.x), math.Abs(p.y), math.Abs(p.z)).
		sub(newPoint(1, 1, 1))
	return newPoint(math.Max(q.x, 0.0),
		math.Max(q.y, 0.0),
		math.Max(q.z, 0.0)).mag() -
		math.Min(math.Max(q.x, math.Max(q.y, q.z)), 0.0)
}

func sphereSDF(p *tuple) float64 {
	return math.Sqrt(p.x*p.x+p.y*p.y+p.z*p.z) - 1.0
}

func displacementFuncWrapper(displacementFunc func(p *tuple) float64) SDFWRapperFunc {
	return func(distFunc func(p *tuple) float64, p *tuple) float64 {
		return distFunc(p) + displacementFunc(p)
	}
}

func twistFuncWrapperfunc(k float64) SDFWRapperFunc {
	return func(distFunc func(p *tuple) float64, p *tuple) float64 {
		c := math.Cos(k * math.Abs(p.y))
		s := math.Sin(k * math.Abs(p.y))
		q := newPoint(c*p.x-s*p.z, p.y, s*p.x+c*p.z)
		return distFunc(q)
	}
}

func bendFuncWrapperfunc(k float64) SDFWRapperFunc {
	return func(distFunc func(p *tuple) float64, p *tuple) float64 {
		c := math.Cos(k * math.Abs(p.x))
		s := math.Sin(k * math.Abs(p.x))
		q := newPoint(c*p.x-s*p.y, c*s+c*p.y, p.z)
		return distFunc(q)
	}
}
