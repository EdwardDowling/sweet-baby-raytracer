package lib

import "testing"

func TestPlane(t *testing.T) {
	p := newPlane()
	r := newRay(newPoint(0, 10, 0), newVector(0, 0, 1))
	xs := p.bo.localIntersects(p, r)
	if len(xs) != 0 {
		t.Error("incorrect result for local intersects of plane")
	}
	r = newRay(newPoint(0, 0, 0), newVector(0, 0, 1))
	xs = p.bo.localIntersects(p, r)
	if len(xs) != 0 {
		t.Error("incorrect result for local intersects of plane")
	}

	r = newRay(newPoint(0, 1, 0), newVector(0, -1, 0))
	xs = p.bo.localIntersects(p, r)
	if len(xs) != 1 {
		t.Error("incorrect result for local intersects of plane")
	}
	if !isClose(xs[0].t, 1) && xs[0].o.equals(p) {
		t.Error("incorrect result for local intersects of plane")
	}

	r = newRay(newPoint(0, -1, 0), newVector(0, 1, 0))
	xs = p.bo.localIntersects(p, r)
	if len(xs) != 1 {
		t.Error("incorrect result for local intersects of plane")
	}
	if !isClose(xs[0].t, 1) && xs[0].o.equals(p) {
		t.Error("incorrect result for local intersects of plane")
	}
}
