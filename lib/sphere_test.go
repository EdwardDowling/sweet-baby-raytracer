package lib

import (
	"math"
	"testing"
)

func TestSphere(t *testing.T) {
	t.Run("basic intersection", func(t *testing.T) {
		s := newSphere()
		r := newRay(newPoint(0, 0, -5), newVector(0, 0, 1))
		intersections := s.intersects(r)
		if len(intersections) != 2 {
			t.Error("incorrect number of intersections for sphere intersection")
		}
		if !isClose(intersections[0].t, 4.0) {
			t.Error("incorrect intersection for sphere intersection")
		}
		if !isClose(intersections[1].t, 6.0) {
			t.Error("incorrect intersection for sphere intersection")
		}
	})
	t.Run("tangent intersection", func(t *testing.T) {
		s := newSphere()
		r := newRay(newPoint(0, 1, -5), newVector(0, 0, 1))
		intersections := s.intersects(r)
		if len(intersections) != 2 {
			t.Error("incorrect number of intersections for sphere intersection")
		}
		if !isClose(intersections[0].t, 5.0) {
			t.Error("incorrect intersection for sphere intersection")
		}
		if !isClose(intersections[1].t, 5.0) {
			t.Error("incorrect intersection for sphere intersection")
		}
	})
	t.Run("no intersection", func(t *testing.T) {
		s := newSphere()
		r := newRay(newPoint(0, 2, -5), newVector(0, 0, 1))
		intersections := s.intersects(r)
		if len(intersections) != 0 {
			t.Error("incorrect number of intersections for sphere intersection")
		}
	})
	t.Run("ray inside sphere", func(t *testing.T) {
		s := newSphere()
		r := newRay(newPoint(0, 0, 0), newVector(0, 0, 1))
		intersections := s.intersects(r)
		if len(intersections) != 2 {
			t.Error("incorrect number of intersections for sphere intersection")
		}
		if !isClose(intersections[0].t, -1.0) {
			t.Error("incorrect intersection for sphere intersection")
		}
		if !isClose(intersections[1].t, 1.0) {
			t.Error("incorrect intersection for sphere intersection")
		}
	})
	t.Run("ray behind sphere", func(t *testing.T) {
		s := newSphere()
		r := newRay(newPoint(0, 0, 5), newVector(0, 0, 1))
		intersections := s.intersects(r)
		if len(intersections) != 2 {
			t.Error("incorrect number of intersections for sphere intersection")
		}
		if !isClose(intersections[0].t, -6.0) {
			t.Error("incorrect intersection for sphere intersection")
		}
		if !isClose(intersections[1].t, -4.0) {
			t.Error("incorrect intersection for sphere intersection")
		}
	})

	// With tranforms
	t.Run("scales sphere", func(t *testing.T) {
		s := newSphere()
		s.setTransform(scaling(2, 2, 2))
		r := newRay(newPoint(0, 0, -5), newVector(0, 0, 1))
		intersections := s.intersects(r)
		if len(intersections) != 2 {
			t.Error("incorrect number of intersections for sphere intersection")
		}
		if !isClose(intersections[0].t, 3) {
			t.Error("incorrect intersection for sphere intersection")
			t.Error(intersections[0].t)
		}
		if !isClose(intersections[1].t, 7) {
			t.Error("incorrect intersection for sphere intersection")
			t.Error(intersections[1].t)
		}
	})
	t.Run("translating sphere", func(t *testing.T) {
		s := newSphere()
		s.setTransform(translation(5, 0, 0))
		r := newRay(newPoint(0, 0, -5), newVector(0, 0, 1))
		intersections := s.intersects(r)
		if len(intersections) != 0 {
			t.Error("incorrect number of intersections for sphere intersection")
		}
	})
}

func TestNormal(t *testing.T) {
	s := newSphere()
	if !s.normalAt(newPoint(1, 0, 0), nil).equals(newVector(1, 0, 0)) {
		t.Error("incorrect value for sphere normal")
	}
	if !s.normalAt(newPoint(0, 1, 0), nil).equals(newVector(0, 1, 0)) {
		t.Error("incorrect value for sphere normal")
	}
	if !s.normalAt(newPoint(0, 0, 1), nil).equals(newVector(0, 0, 1)) {
		t.Error("incorrect value for sphere normal")
	}
	v := math.Sqrt(3) / 3.0
	if !s.normalAt(newPoint(v, v, v), nil).equals(newVector(v, v, v)) {
		t.Error("incorrect value for sphere normal")
	}

	s.setTransform(translation(0, 1, 0))
	n := s.normalAt(newPoint(0, 1.70711, -0.70711), nil)
	vec := newVector(0, 0.7071067811865475, -0.7071067811865476)
	if !n.equals(vec) {
		t.Error("incorrect value for sphere normal")
	}
}
