package lib

import "testing"

func TestColors(t *testing.T) {
	c1 := newColor(0.9, 0.6, 0.75)
	c2 := newColor(0.7, 0.1, 0.25)
	if !c1.add(c2).equals(newColor(1.6, 0.7, 1.0)) {
		t.Errorf("unexpected result for color addition")
	}
	if !c1.sub(c2).equals(newColor(0.2, 0.5, 0.5)) {
		t.Errorf("unexpected result for color subtraction")
	}
	c3 := newColor(0.2, 0.3, 0.4)
	if !c3.mul(2).equals(newColor(0.4, 0.6, 0.8)) {
		t.Errorf("unexpected result for color multiplication")
	}
	c4 := newColor(1, 0.2, 0.4)
	c5 := newColor(0.9, 1, 0.1)
	if !c4.hamardProduct(c5).equals(newColor(0.9, 0.2, 0.04)) {
		t.Errorf("unexpected result for color hamard product")
	}
}
