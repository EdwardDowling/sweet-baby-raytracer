package lib

import "math"

type tuple struct {
	x float64
	y float64
	z float64
	w float64 // Vector = 0 & Point = 1
}

func newTuple(x, y, z, w float64) *tuple {
	return &tuple{
		x: x,
		y: y,
		z: z,
		w: w,
	}
}

func newVector(x, y, z float64) *tuple {
	return newTuple(x, y, z, 0)
}

func newPoint(x, y, z float64) *tuple {
	return newTuple(x, y, z, 1)
}

func (t *tuple) isVector() bool {
	return t.w == 0
}

func (t *tuple) isPoint() bool {
	return t.w == 1
}

func (t *tuple) equals(o *tuple) bool {
	return isClose(t.x, o.x) &&
		isClose(t.y, o.y) &&
		isClose(t.z, o.z) &&
		isClose(t.w, o.w)
}

func (t *tuple) add(o *tuple) *tuple {
	return newTuple(t.x+o.x, t.y+o.y, t.z+o.z, t.w+o.w)
}

func (t *tuple) sub(o *tuple) *tuple {
	return newTuple(t.x-o.x, t.y-o.y, t.z-o.z, t.w-o.w)
}

func (t *tuple) negate() *tuple {
	return newTuple(-t.x, -t.y, -t.z, -t.w)
}

func (t *tuple) mul(s float64) *tuple {
	return newTuple(t.x*s, t.y*s, t.z*s, t.w*s)
}

func (t *tuple) div(s float64) *tuple {
	return newTuple(t.x/s, t.y/s, t.z/s, t.w/s)
}

func (t *tuple) mag() float64 {
	return math.Sqrt(t.x*t.x + t.y*t.y + t.z*t.z)
}

func (t *tuple) normalize() *tuple {
	invM := 1 / t.mag()
	return newTuple(t.x*invM, t.y*invM, t.z*invM, t.w*invM)
}

func (t *tuple) dot(o *tuple) float64 {
	return t.x*o.x + t.y*o.y + t.z*o.z + t.w*o.w
}

func (t *tuple) cross(o *tuple) *tuple {
	return newVector(t.y*o.z-t.z*o.y,
		t.z*o.x-t.x*o.z,
		t.x*o.y-t.y*o.x)
}

func (t *tuple) copy() *tuple {
	return &tuple{
		x: t.x,
		y: t.y,
		z: t.z,
		w: t.w,
	}
}

func (t *tuple) reflect(normal *tuple) *tuple {
	return t.sub(normal.mul(2 * t.dot(normal)))
}
