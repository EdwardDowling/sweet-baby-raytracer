package lib

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"strconv"
	"strings"
)

var (
	INVALID_OBJ = errors.New("malformed obj file")
)

func parseObj(r io.Reader) (*object, error) {
	scanner := bufio.NewScanner(r)
	objects := []*object{}
	vertices := []*tuple{}
	normals := []*tuple{}
	for scanner.Scan() {
		line := scanner.Text()
		tokens := strings.Split(line, " ")
		if len(tokens) < 1 {
			continue
		}
		switch tokens[0] {
		case "v":
			// Vertex x y z
			if len(tokens) != 4 {
				return nil, fmt.Errorf(line+"%w", INVALID_OBJ)
			}
			x, err := strconv.ParseFloat(tokens[1], 64)
			if err != nil {
				return nil, fmt.Errorf(line+"%w", INVALID_OBJ)
			}
			y, err := strconv.ParseFloat(tokens[2], 64)
			if err != nil {
				return nil, fmt.Errorf(line+"%w", INVALID_OBJ)
			}
			z, err := strconv.ParseFloat(tokens[3], 64)
			if err != nil {
				return nil, fmt.Errorf(line+"%w", INVALID_OBJ)
			}
			vertices = append(vertices, newPoint(x, y, z))
			continue
		case "f":
			currentVertices := []*tuple{}
			currentNormals := []*tuple{}
			for _, v := range tokens[1:] {
				// f a/b/c
				// a == vertex index
				// b == texture index (ignored)
				// c == normal index
				vertex := strings.Split(v, "/")
				v := vertex[0]
				vertexIndex, err := strconv.ParseInt(v, 10, 64)
				if err != nil {
					return nil, fmt.Errorf(line+"%w", INVALID_OBJ)
				}
				if len(vertex) != 3 {
					currentVertices = append(currentVertices, vertices[vertexIndex-1])
					currentNormals = append(currentNormals, newVector(0, 0, 0))
					continue
				}
				currentVertices = append(currentVertices, vertices[vertexIndex-1])
				n := vertex[2]
				normalIndex, err := strconv.ParseInt(n, 10, 64)
				if err != nil {
					return nil, fmt.Errorf(line+"%w", INVALID_OBJ)
				}
				currentNormals = append(currentNormals, normals[normalIndex-1])
				continue
			}
			objects = append(objects,
				verticesToObjects(currentVertices, currentNormals)...)
			continue
		case "vn":
			if len(tokens) != 4 {
				return nil, fmt.Errorf(line+"%w", INVALID_OBJ)
			}
			x, err := strconv.ParseFloat(tokens[1], 64)
			if err != nil {
				return nil, fmt.Errorf(line+"%w", INVALID_OBJ)
			}
			y, err := strconv.ParseFloat(tokens[2], 64)
			if err != nil {
				return nil, fmt.Errorf(line+"%w", INVALID_OBJ)
			}
			z, err := strconv.ParseFloat(tokens[3], 64)
			if err != nil {
				return nil, fmt.Errorf(line+"%w", INVALID_OBJ)
			}
			normals = append(normals, newVector(x, y, z))
			continue
		}
	}

	g := newGroup()
	g.addObjects(objects)
	return g, nil
}

func verticesToObjects(vertices, normals []*tuple) []*object {
	triangles := []*object{}
	nilVector := newVector(0, 0, 0)
	for index := 1; index < len(vertices)-1; index++ {
		// TODO: Make this not gross, this is janky as hell
		if !normals[0].equals(nilVector) {
			tri := newSmoothTriangle(vertices[0], vertices[index], vertices[index+1],
				normals[0], normals[index], normals[index+1])
			triangles = append(triangles, tri)
			continue
		}
		tri := newTriangle(vertices[0], vertices[index], vertices[index+1])
		triangles = append(triangles, tri)
	}
	return triangles
}
