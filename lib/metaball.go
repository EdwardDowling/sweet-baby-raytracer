package lib

import (
	"math"
)

// http://www.geisswerks.com/ryan/BLOBS/blobs.html

type metaball struct {
	objects    []*object
	thresholds []float64

	lowerBounds *tuple
	upperBounds *tuple
}

func newMetaball(objects []*object, threshold float64) *object {
	o := newObject()
	thresholds := make([]float64, len(objects))
	for i := range objects {
		thresholds[i] = threshold
	}
	mb := &metaball{
		objects:    objects,
		thresholds: thresholds,
	}
	lower, upper := mb.bounds()
	mb.lowerBounds = lower
	mb.upperBounds = upper
	o.bo = mb
	return o
}

func (mb *metaball) localIntersects(ob *object, r *ray) []*intersection {
	// TODO: Add all intersections what for transparency to work
	xs := []*intersection{}
	if !mb.intersectsBounds(ob, r) {
		return xs
	}
	if t, ok := mb.getTForRay(r); ok {
		return []*intersection{newIntersection(t, ob)}
	}
	return []*intersection{}
}

func (mb *metaball) getTForRay(r *ray) (float64, bool) {
	depth := 0.0
	dir := r.direction.normalize()
	mag := r.direction.mag()
	for i := 0; i < MAX_MARCHING_STEPS; i++ {
		p := r.origin.add(dir.mul(depth))
		dist := mb.sdf(p)
		if isThisClose(dist, 0.0, EPSILON/1000) {
			return depth / mag, true
		}
		depth += dist
		if depth > MAX_MARCHIN_DIST {
			return 0.0, false
		}
	}
	return 0.0, false
}

func (mb *metaball) sdf(p *tuple) float64 {
	min := INFINITY
	first := true
	for i, o := range mb.objects {
		dist := o.sdf(p)
		if first {
			min = dist
			first = false
			continue
		}
		min = smin(min, dist, mb.thresholds[i])
	}
	return min
}

func (mb *metaball) localNormalAt(p *tuple, _ *intersection) *tuple {
	someSmallValue := EPSILON
	x := mb.sdf(newPoint(p.x+someSmallValue, p.y, p.z)) -
		mb.sdf(newPoint(p.x-someSmallValue, p.y, p.z))

	y := mb.sdf(newPoint(p.x, p.y+someSmallValue, p.z)) -
		mb.sdf(newPoint(p.x, p.y-someSmallValue, p.z))

	z := mb.sdf(newPoint(p.x, p.y, p.z+someSmallValue)) -
		mb.sdf(newPoint(p.x, p.y, p.z-someSmallValue))
	// fmt.Println(mb.sdf(newPoint(p.x+someSmallValue, p.y, p.z)), y, z)
	return newVector(x, y, z).normalize()
}

func (mb *metaball) equals(other baseObject) bool {
	omb, ok := other.(*metaball)
	if !ok {
		return false
	}
	for i, p := range omb.objects {
		if !mb.objects[i].equals(p) {
			return false
		}
	}
	return ok
}

func (mb *metaball) intersectsBounds(_ *object, r *ray) bool {
	minBounds := mb.lowerBounds
	maxBounds := mb.upperBounds

	xTMin, xTMax := mb.checkAxis(r.origin.x, r.direction.x, minBounds.x, maxBounds.x)
	yTMin, yTMax := mb.checkAxis(r.origin.y, r.direction.y, minBounds.y, maxBounds.y)
	zTMin, zTMax := mb.checkAxis(r.origin.z, r.direction.z, minBounds.z, maxBounds.z)

	tMin := math.Max(xTMin, math.Max(yTMin, zTMin))
	tMax := math.Min(xTMax, math.Min(yTMax, zTMax))

	if tMin > tMax || tMax < 0.0 {
		return false
	}
	return true
}

func (mb *metaball) checkAxis(origin, direction, min, max float64) (float64, float64) {
	// Offset plane intersection
	tMinNumerator := (min - origin)
	tMaxNumerator := (max - origin)
	var tMin float64
	var tMax float64
	if math.Abs(direction) >= EPSILON {
		tMin = tMinNumerator / direction
		tMax = tMaxNumerator / direction
	} else {
		tMin = tMinNumerator * INFINITY
		tMax = tMaxNumerator * INFINITY
	}
	if tMin > tMax {
		tMin, tMax = tMax, tMin
	}
	return tMin, tMax
}

func (mb *metaball) bounds() (*tuple, *tuple) {
	minX := -INFINITY
	minY := -INFINITY
	minZ := -INFINITY

	maxX := INFINITY
	maxY := INFINITY
	maxZ := INFINITY

	for _, child := range mb.objects {
		childMin, childMax := child.bo.bounds()
		localMin := child.getTransform().mulByTuple(childMin)
		localMax := child.getTransform().mulByTuple(childMax)

		minX = math.Min(minX, localMin.x)
		minY = math.Min(minY, localMin.y)
		minZ = math.Min(minZ, localMin.z)

		maxX = math.Max(maxX, localMax.x)
		maxY = math.Max(maxY, localMax.y)
		maxZ = math.Max(maxZ, localMax.z)
	}
	return newPoint(minX, minY, minZ), newPoint(maxX, maxY, maxZ)
}

func (mb *metaball) getLines() []*line {
	return nil
}
