package lib

import (
	"math"
)

type sphere struct{}

func newSphere() *object {
	o := newObject()
	o.bo = &sphere{}
	return o
}

func (s *sphere) localIntersects(ob *object, r *ray) []*intersection {
	sphereToRay := r.origin.sub(newPoint(0, 0, 0))
	a := r.direction.dot(r.direction)
	b := 2.0 * r.direction.dot(sphereToRay)
	c := sphereToRay.dot(sphereToRay) - 1
	discriminant := b*b - 4.0*a*c
	if discriminant < 0 {
		return []*intersection{}
	}
	t1 := (-b - math.Sqrt(discriminant)) / (2.0 * a)
	t2 := (-b + math.Sqrt(discriminant)) / (2.0 * a)
	if t1 < t2 {
		return []*intersection{
			newIntersection(t1, ob),
			newIntersection(t2, ob),
		}
	}
	return []*intersection{
		newIntersection(t2, ob),
		newIntersection(t1, ob),
	}
}

func (s *sphere) sdf(p *tuple) float64 {
	return math.Sqrt(p.x*p.x+p.y*p.y+p.z*p.z) - 1.0
}

func (s *sphere) localNormalAt(p *tuple, _ *intersection) *tuple {
	return p.sub(newPoint(0, 0, 0)).normalize()
}

func (s *sphere) equals(other baseObject) bool {
	_, ok := other.(*sphere)
	return ok
}

func (s *sphere) bounds() (*tuple, *tuple) {
	return newPoint(-1, -1, -1), newPoint(1, 1, 1)
}

func (s *sphere) getLines() []*line {
	return nil
}
