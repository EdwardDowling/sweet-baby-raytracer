package lib

import (
	"math"
	"testing"
)

func TestCameratransform(t *testing.T) {
	from := newPoint(0, 0, 8)
	to := newPoint(0, 0, 0)
	up := newVector(0, 1, 0)
	vt := newViewTransformation(from, to, up)
	if !vt.equals(translation(0, 0, -8)) {
		t.Error("incorrect result from camera translate")
	}
}

func TestCamera(t *testing.T) {
	hSize := 160
	vSize := 120
	fov := math.Pi / 2.0
	c := newCamera(hSize, vSize, fov)
	if c.hSize != hSize {
		t.Error("incorrect result from camera init")
	}
	if c.vSize != vSize {
		t.Error("incorrect result from camera init")
	}
	if c.fov != fov {
		t.Error("incorrect result from camera init")
	}
	if !c.transform.equals(identity()) {
		t.Error("incorrect result from camera init")
	}

	c = newCamera(200, 125, math.Pi/2.0)
	if !isClose(c.pixelSize, 0.01) {
		t.Error("incorrect result from camera init")
	}
	c = newCamera(125, 200, math.Pi/2.0)
	if !isClose(c.pixelSize, 0.01) {
		t.Error("incorrect result from camera init")
	}
}

func TestRayForPixel(t *testing.T) {
	c := newCamera(201, 101, math.Pi/2.0)
	r := c.rayForPixel(100, 50)
	if !r.origin.equals(newPoint(0, 0, 0)) {
		t.Error("incorrect origin for rayForPixel")
	}
	if !r.direction.equals(newVector(0, 0, -1)) {
		t.Error("incorrect direction for rayForPixel")
	}

	c.setTransform(translation(0, -2, 5).rotateY(math.Pi / 4.0))
	r = c.rayForPixel(100, 50)

	if !r.origin.equals(newPoint(0, 2, -5)) {
		t.Error("incorrect origin for rayForPixel")
	}
	if !r.direction.equals(newVector(math.Sqrt2/2.0, 0, -math.Sqrt2/2.0)) {
		t.Error("incorrect direction for rayForPixel")
	}
}

func TestRender(t *testing.T) {
	w := defaultWorld()
	c := newCamera(11, 11, math.Pi/2.0)
	from := newPoint(0, 0, -5)
	to := newPoint(0, 0, 0)
	up := newVector(0, 1, 0)
	c.setTransform(newViewTransformation(from, to, up))
	image := c.render(w)
	if !image.get(5, 5).equals(newColor(0.38066119308103435,
		0.47582649135129296, 0.28549589481077575)) {
		t.Error("incorrect result for default world render")
	}
}
