package lib

import "fmt"

type color struct {
	// 0 -> 1 = 0 -> 255
	r float64
	g float64
	b float64
}

func newColor(r, g, b float64) *color {
	return &color{
		r: r,
		g: g,
		b: b,
	}
}

func newRGBColor(r, g, b float64) *color {
	return &color{
		r: r / 255.0,
		g: g / 255.0,
		b: b / 255.0,
	}
}

func (c *color) add(o *color) *color {
	return newColor(c.r+o.r, c.g+o.g, c.b+o.b)
}

func (c *color) sub(o *color) *color {
	return newColor(c.r-o.r, c.g-o.g, c.b-o.b)
}

func (c *color) equals(o *color) bool {
	return isClose(c.r, o.r) &&
		isClose(c.g, o.g) &&
		isClose(c.b, o.b)
}

func (c *color) mul(s float64) *color {
	return newColor(c.r*s, c.g*s, c.b*s)
}

func (c *color) hamardProduct(o *color) *color {
	return newColor(c.r*o.r, c.g*o.g, c.b*o.b)
}

func (c *color) toStrings() []string {
	r := c.r
	g := c.g
	b := c.b
	if r > 1.0 {
		r = 1.0
	}
	if g > 1.0 {
		g = 1.0
	}
	if b > 1.0 {
		b = 1.0
	}
	return []string{fmt.Sprintf("%d", int(255*r)),
		fmt.Sprintf("%d", int(255*g)),
		fmt.Sprintf("%d", int(255*b)),
	}
}
