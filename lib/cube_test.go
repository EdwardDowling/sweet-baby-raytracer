package lib

import "testing"

func TestCubeIntersects(t *testing.T) {
	c := newCube()
	for index, tc := range []struct {
		origin    *tuple
		direction *tuple
		t1        float64
		t2        float64
	}{
		{
			origin:    newPoint(5, 0.5, 0),
			direction: newVector(-1, 0, 0),
			t1:        4.0,
			t2:        6.0,
		},
		{
			origin:    newPoint(-5, 0.5, 0),
			direction: newVector(1, 0, 0),
			t1:        4.0,
			t2:        6.0,
		},
		{
			origin:    newPoint(0.5, 5, 0),
			direction: newVector(0, -1, 0),
			t1:        4.0,
			t2:        6.0,
		},
		{
			origin:    newPoint(0.5, -5, 0),
			direction: newVector(0, 1, 0),
			t1:        4.0,
			t2:        6.0,
		},
		{
			origin:    newPoint(0.5, 0, 5),
			direction: newVector(0, 0, -1),
			t1:        4.0,
			t2:        6.0,
		},
		{
			origin:    newPoint(0.5, 0, -5),
			direction: newVector(0, 0, 1),
			t1:        4.0,
			t2:        6.0,
		},
		{
			origin:    newPoint(0, 0.5, 0),
			direction: newVector(0, 0, 1),
			t1:        -1,
			t2:        1,
		},
	} {
		r := newRay(tc.origin, tc.direction)
		xs := c.bo.localIntersects(c, r)
		if len(xs) != 2 {
			t.Errorf("incorrect number of intersections tc: %d", index)
			continue
		}
		if !isClose(tc.t1, xs[0].t) {
			t.Errorf("incorrect value for first intersection tc: %d", index)
		}
		if !isClose(tc.t2, xs[1].t) {
			t.Errorf("incorrect value for second intersection tc: %d", index)
		}
	}
}
