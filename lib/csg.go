package lib

import (
	"math"
)

type operand string

const (
	UNION        operand = "union"
	INTERSECTION operand = "intersection"
	DIFFERENCE   operand = "difference"
)

type csg struct {
	op                       operand
	left, right              *object
	lowerBounds, upperBounds *tuple
}

func unionOf(a, b *object) *object {
	o := newObject()
	c := &csg{
		op:    UNION,
		left:  a,
		right: b,
	}
	c.lowerBounds, c.upperBounds = c.calcBounds()
	o.bo = c
	a.parent = o
	b.parent = o
	return o
}

func intersectionOf(a, b *object) *object {
	o := newObject()
	c := &csg{
		op:    INTERSECTION,
		left:  a,
		right: b,
	}
	c.lowerBounds, c.upperBounds = c.calcBounds()
	o.bo = c
	a.parent = o
	b.parent = o
	return o
}

func differenceOf(a, b *object) *object {
	o := newObject()
	c := &csg{
		op:    DIFFERENCE,
		left:  a,
		right: b,
	}
	c.lowerBounds, c.upperBounds = c.calcBounds()
	o.bo = c
	a.parent = o
	b.parent = o
	return o
}

func newCsg() *object {
	o := newObject()
	o.bo = &csg{}
	return o
}

func intersectionAllowed(op operand, lHit, inL, inR bool) bool {
	switch op {
	case UNION:
		return (lHit && !inR) || (!lHit && !inL)
	case INTERSECTION:
		return (lHit && inR) || (!lHit && inL)
	case DIFFERENCE:
		return (lHit && !inR) || (!lHit && inL)
	}
	return false
}

func (c *csg) filterIntersections(xs []*intersection) []*intersection {
	inL, inR := false, false
	ret := []*intersection{}
	for _, x := range xs {
		lHit := c.left.includes(x.o)
		if intersectionAllowed(c.op, lHit, inL, inR) {
			ret = append(ret, x)
		}
		if lHit {
			inL = !inL
		} else {
			inR = !inR
		}
	}
	return ret
}

func (c *csg) includes(other *object) bool {
	if c.equals(other.bo) {
		return true
	}
	if c.left != nil && c.left.includes(other) {
		return true
	}
	if c.right != nil && c.right.includes(other) {
		return true
	}
	return false
}

func (c *csg) equals(other baseObject) bool {
	o, ok := other.(*csg)
	if !ok {
		return false
	}
	if o.op != c.op {
		return false
	}
	if !((c.left != nil) == (o.left != nil)) {
		return false
	}
	if !((c.right != nil) == (o.right != nil)) {
		return false
	}
	if c.left != nil {
		if !c.left.equals(o.left) {
			return false
		}
	}
	if c.right != nil {
		if !c.right.equals(o.right) {
			return false
		}
	}
	return true
}

func (c *csg) localNormalAt(p *tuple, hit *intersection) *tuple {
	panic(1)
	return nil
}

func (c *csg) localIntersects(o *object, r *ray) []*intersection {
	if !c.intersectsBounds(o, r) {
		return []*intersection{}
	}

	leftXs := []*intersection{}
	if c.left != nil {
		leftXs = c.left.intersects(r)
	}
	rightXs := []*intersection{}
	if c.right != nil {
		rightXs = c.right.intersects(r)
	}
	results := []*intersection{}
	// TODO: Sort these by t in place instead of this crap
	for index := 0; index < len(rightXs); index++ {
		results = insertIntersection(rightXs[index], results)
	}
	for index := 0; index < len(leftXs); index++ {
		results = insertIntersection(leftXs[index], results)
	}

	return c.filterIntersections(results)
}

func (c *csg) intersectsBounds(ob *object, r *ray) bool {
	minBounds := c.lowerBounds
	maxBounds := c.upperBounds

	xTMin, xTMax := c.checkAxis(r.origin.x, r.direction.x, minBounds.x, maxBounds.x)
	yTMin, yTMax := c.checkAxis(r.origin.y, r.direction.y, minBounds.y, maxBounds.y)
	zTMin, zTMax := c.checkAxis(r.origin.z, r.direction.z, minBounds.z, maxBounds.z)

	tMin := math.Max(xTMin, math.Max(yTMin, zTMin))
	tMax := math.Min(xTMax, math.Min(yTMax, zTMax))

	if tMin > tMax || tMax < 0.0 {
		return false
	}
	return true
}

func (c *csg) checkAxis(origin, direction, min, max float64) (float64, float64) {
	// Offset plane intersection
	tMinNumerator := (min - origin)
	tMaxNumerator := (max - origin)
	var tMin float64
	var tMax float64
	if math.Abs(direction) >= EPSILON {
		tMin = tMinNumerator / direction
		tMax = tMaxNumerator / direction
	} else {
		tMin = tMinNumerator * INFINITY
		tMax = tMaxNumerator * INFINITY
	}
	if tMin > tMax {
		tMin, tMax = tMax, tMin
	}
	return tMin, tMax
}

func (c *csg) bounds() (*tuple, *tuple) {
	return c.lowerBounds, c.upperBounds
}

func (c *csg) calcBounds() (*tuple, *tuple) {
	// TODO: Replace this with something better than reusing the group bounds
	minX := -INFINITY
	minY := -INFINITY
	minZ := -INFINITY

	maxX := INFINITY
	maxY := INFINITY
	maxZ := INFINITY

	for _, child := range []*object{c.left, c.right} {
		if child == nil {
			continue
		}
		childMin, childMax := child.bo.bounds()
		localMin := child.getTransform().mulByTuple(childMin)
		localMax := child.getTransform().mulByTuple(childMax)

		minX = math.Min(minX, localMin.x)
		minY = math.Min(minY, localMin.y)
		minZ = math.Min(minZ, localMin.z)

		maxX = math.Max(maxX, localMax.x)
		maxY = math.Max(maxY, localMax.y)
		maxZ = math.Max(maxZ, localMax.z)
	}
	return newPoint(minX, minY, minZ), newPoint(maxX, maxY, maxZ)
}

func (c *csg) sdf(p *tuple) float64 {
	panic(1)
}

func (c *csg) getLines() []*line {
	lines := []*line{}
	if c.left != nil {
		lines = append(lines, c.left.getLines()...)
	}
	if c.right != nil {
		lines = append(lines, c.right.getLines()...)
	}
	return lines
}

func csgToString(o *object) string {
	c, ok := o.bo.(*csg)
	if !ok {
		return "not a csg"
	}
	return string(c.op) + " (" + csgToString(c.right) + ") (" + csgToString(c.left) + ")"
}
