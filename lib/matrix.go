package lib

import (
	"fmt"
	"math"
)

// Only need 4*4 3*3 and 2*2 matrices
// Assuming all matrices are one of these
type matrix [][]float64

func identity() matrix {
	return matrix{
		{1, 0, 0, 0},
		{0, 1, 0, 0},
		{0, 0, 1, 0},
		{0, 0, 0, 1},
	}
}

func (m matrix) equals(o matrix) bool {
	if len(m) != len(o) {
		return false
	}
	for x := 0; x < len(m); x++ {
		for y := 0; y < len(m[0]); y++ {
			if !isClose(m[x][y], o[x][y]) {
				return false
			}
		}
	}
	return true
}

func (m matrix) mul(o matrix) matrix {
	ret := make(matrix, len(m))
	for i := range m {
		ret[i] = make([]float64, len(m))
	}
	for y := range m {
		for x := range m {
			for i := range m {
				ret[x][y] += m[x][i] * o[i][y]
			}
		}
	}
	return ret
}

// Only applicable to 4*4 matrices
func (m matrix) mulByTuple(t *tuple) *tuple {
	return &tuple{
		x: t.x*m[0][0] + t.y*m[0][1] + t.z*m[0][2] + t.w*m[0][3],
		y: t.x*m[1][0] + t.y*m[1][1] + t.z*m[1][2] + t.w*m[1][3],
		z: t.x*m[2][0] + t.y*m[2][1] + t.z*m[2][2] + t.w*m[2][3],
		w: t.x*m[3][0] + t.y*m[3][1] + t.z*m[3][2] + t.w*m[3][3],
	}
}

func (m matrix) transpose() matrix {
	ret := make(matrix, len(m))
	for i := range m {
		ret[i] = make([]float64, len(m))
	}
	for x := range m {
		for y := range m {
			ret[y][x] = m[x][y]
		}
	}
	return ret
}

func (m matrix) toString() string {
	ret := ""
	for x := range m {
		for y := range m {
			ret = fmt.Sprintf(ret+"%f ", m[x][y])
		}
		ret += "\n"
	}
	return ret
}

func (m matrix) determinant() float64 {
	if len(m) == 2 {
		return m[0][0]*m[1][1] - m[1][0]*m[0][1]
	}
	determinant := 0.0
	for x := range m {
		determinant += m[x][0] * m.cofactor(x, 0)
	}
	return determinant
}

func (m matrix) subMatrix(notX, notY int) matrix {
	ret := make(matrix, len(m)-1)
	for x := range ret {
		ret[x] = make([]float64, len(m)-1)
	}
	retX := 0
	retY := 0
	for x := 0; x < len(m); x++ {
		if x == notX {
			continue
		}
		for y := 0; y < len(m); y++ {
			if y == notY {
				continue
			}
			ret[retX][retY] = m[x][y]
			retY++
		}
		retY = 0
		retX++
	}
	return ret
}

func (m matrix) minor(x, y int) float64 {
	return m.subMatrix(x, y).determinant()
}

func (m matrix) cofactor(x, y int) float64 {
	return m.minor(x, y) * float64(((x+y)%2)*-2+1)
}

// Only matrices with non 0 determinant are invertable
// leaving out checking for this for speed
// will add back in if i end up chekcing for this later
func (m matrix) inverse() matrix {
	ret := make(matrix, len(m))
	for i := range m {
		ret[i] = make([]float64, len(m))
	}
	invDeterminant := 1 / m.determinant()
	for x := range m {
		for y := range m {
			ret[y][x] = m.cofactor(x, y) * invDeterminant
		}
	}
	return ret
}

func translation(x, y, z float64) matrix {
	return matrix{
		{1, 0, 0, x},
		{0, 1, 0, y},
		{0, 0, 1, z},
		{0, 0, 0, 1},
	}
}

func scaling(x, y, z float64) matrix {
	return matrix{
		{x, 0, 0, 0},
		{0, y, 0, 0},
		{0, 0, z, 0},
		{0, 0, 0, 1},
	}
}

func rotationX(r float64) matrix {
	return matrix{
		{1, 0, 0, 0},
		{0, math.Cos(r), -math.Sin(r), 0},
		{0, math.Sin(r), math.Cos(r), 0},
		{0, 0, 0, 1},
	}
}

func rotationY(r float64) matrix {
	return matrix{
		{math.Cos(r), 0, math.Sin(r), 0},
		{0, 1, 0, 0},
		{-math.Sin(r), 0, math.Cos(r), 0},
		{0, 0, 0, 1},
	}
}

func rotationZ(r float64) matrix {
	return matrix{
		{math.Cos(r), -math.Sin(r), 0, 0},
		{math.Sin(r), math.Cos(r), 0, 0},
		{0, 0, 1, 0},
		{0, 0, 0, 1},
	}
}

func shearing(xy, xz, yx, yz, zx, zy float64) matrix {
	return matrix{
		{1, xy, xz, 0},
		{yx, 1, yz, 0},
		{zx, zy, 1, 0},
		{0, 0, 0, 1},
	}
}

func (m matrix) translate(x, y, z float64) matrix {
	return translation(x, y, z).mul(m)
}

func (m matrix) scale(x, y, z float64) matrix {
	return scaling(x, y, z).mul(m)
}

func (m matrix) rotateX(r float64) matrix {
	return rotationX(r).mul(m)
}

func (m matrix) rotateY(r float64) matrix {
	return rotationY(r).mul(m)
}

func (m matrix) rotateZ(r float64) matrix {
	return rotationZ(r).mul(m)
}

func (m matrix) shear(xy, xz, yx, yz, zx, zy float64) matrix {
	return shearing(xy, xz, yx, yz, zx, zy).mul(m)
}
