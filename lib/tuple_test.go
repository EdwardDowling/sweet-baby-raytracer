package lib

import (
	"math"
	"testing"
)

func TestNewVector(t *testing.T) {
	v := newVector(0, 0, 0)
	if v.w != 0 {
		t.Error("vector w component should be 0")
	}
}

func TestNewPoint(t *testing.T) {
	v := newPoint(0, 0, 0)
	if v.w != 1 {
		t.Error("point w component should be 1")
	}
}

func TestIsVector(t *testing.T) {
	tp := &tuple{
		x: 1,
		y: 1,
		z: 1,
		w: 0,
	}
	if !tp.isVector() {
		t.Error("isVector should be true when w is 0")
	}
}

func TestIsPoint(t *testing.T) {
	tp := &tuple{
		x: 1,
		y: 1,
		z: 1,
		w: 1,
	}
	if !tp.isPoint() {
		t.Error("isPoint should be true when w is 1")
	}
}

func TestAdd(t *testing.T) {
	t1 := newPoint(3, -2, 5)
	t2 := newVector(-2, 3, 1)
	t3 := t1.add(t2)
	want := newPoint(1, 1, 6)
	if !t3.equals(want) {
		t.Error("unexpected result for tuple addition")
	}
}

func TestSub(t *testing.T) {
	t1 := newPoint(3, 2, 1)
	t2 := newPoint(5, 6, 7)
	t3 := t1.sub(t2)
	want := newVector(-2, -4, -6)
	if !t3.equals(want) {
		t.Error("unexpected result for tuple subtraction")
	}

	t1 = newVector(3, 2, 1)
	t2 = newVector(5, 6, 7)
	t3 = t1.sub(t2)
	want = newVector(-2, -4, -6)
	if !t3.equals(want) {
		t.Error("unexpected result for tuple subtraction")
	}

	t1 = newPoint(3, 2, 1)
	t2 = newVector(5, 6, 7)
	t3 = t1.sub(t2)
	want = newPoint(-2, -4, -6)
	if !t3.equals(want) {
		t.Error("unexpected result for tuple subtraction")
	}
}

func TestNegation(t *testing.T) {
	t1 := newVector(1, -2, 3)
	want := newVector(-1, 2, -3)
	if !t1.negate().equals(want) {
		t.Error("unexpected result for tuple negation")
	}
}

func TestMultiplication(t *testing.T) {
	t1 := newTuple(1, -2, 3, -4)
	want := newTuple(3.5, -7, 10.5, -14)
	if !t1.mul(3.5).equals(want) {
		t.Errorf("unexpected result for tuple multiplication")
	}
}

func TestDivision(t *testing.T) {
	t1 := newTuple(1, -2, 3, -4)
	want := newTuple(0.5, -1, 1.5, -2)
	if !t1.div(2).equals(want) {
		t.Errorf("unexpected result for tuple division")
	}
}

func TestMagnitude(t *testing.T) {
	t1 := newVector(1, 0, 0)
	if !isClose(t1.mag(), 1) {
		t.Errorf("unexpected result for tuple magnitude")
	}
	t1 = newVector(0, 1, 0)
	if !isClose(t1.mag(), 1) {
		t.Errorf("unexpected result for tuple magnitude")
	}
	t1 = newVector(0, 0, 1)
	if !isClose(t1.mag(), 1) {
		t.Errorf("unexpected result for tuple magnitude")
	}
	t1 = newVector(1, 2, 3)
	if !isClose(t1.mag(), math.Sqrt(14)) {
		t.Errorf("unexpected result for tuple magnitude")
	}
	t1 = newVector(-1, -2, -3)
	if !isClose(t1.mag(), math.Sqrt(14)) {
		t.Errorf("unexpected result for tuple magnitude")
	}
}

func TestNormalize(t *testing.T) {
	t1 := newVector(4, 0, 0)
	if !isClose(t1.normalize().mag(), 1) {
		t.Errorf("unexpected result for tuple normalize")
	}
	t1 = newVector(1, 2, 3)
	if !isClose(t1.normalize().mag(), 1) {
		t.Errorf("unexpected result for tuple normalize")
	}
	want := newVector(1/math.Sqrt(14), 2/math.Sqrt(14), 3/math.Sqrt(14))
	if !t1.normalize().equals(want) {
		t.Errorf("unexpected result for tuple normalize")
	}
}

func TestDot(t *testing.T) {
	t1 := newVector(1, 2, 3)
	t2 := newVector(2, 3, 4)
	if !isClose(t1.dot(t2), 20) {
		t.Errorf("unexpected result for tuple dot product")
	}
}

func TestCross(t *testing.T) {
	t1 := newVector(1, 2, 3)
	t2 := newVector(2, 3, 4)
	if !t1.cross(t2).equals(newVector(-1, 2, -1)) {
		t.Errorf("unexpected result for tuple cross product")
	}
	if !t2.cross(t1).equals(newVector(1, -2, 1)) {
		t.Errorf("unexpected result for tuple cross product")
	}
}

func TestReflect(t *testing.T) {
	v := newVector(1, -1, 0)
	n := newVector(0, 1, 0)
	if !newVector(1, 1, 0).equals(v.reflect(n)) {
		t.Error("incorrect result of reflection around normal")
	}

	v = newVector(0, -1, 0)
	n = newVector(math.Sqrt2/2, math.Sqrt2/2, 0)
	if !newVector(1, 0, 0).equals(v.reflect(n)) {
		t.Error("incorrect result of reflection around normal")
	}
}
