package lib

type baseObject interface {
	localNormalAt(p *tuple, hit *intersection) *tuple
	localIntersects(o *object, r *ray) []*intersection
	sdf(p *tuple) float64
	equals(o baseObject) bool
	bounds() (*tuple, *tuple)
	getLines() []*line
}

type object struct {
	transform              matrix
	invTransform           matrix
	invTransformTransposed matrix
	hasTransform           bool

	material       *material
	bo             baseObject
	parent         *object
	normalFuncHook normalFuncHook
}

func newObject() *object {
	return &object{
		transform:              identity(),
		invTransform:           identity().inverse(),
		invTransformTransposed: identity().inverse().transpose(),
		material:               defaultMaterial(),
		normalFuncHook:         defaultNormalFuncHook,
	}
}

func (ob *object) worldToObject(p *tuple) *tuple {
	if ob.parent != nil {
		p = ob.parent.worldToObject(p)
	}
	return ob.invTransform.mulByTuple(p)
}

func (ob *object) sdf(p *tuple) float64 {
	p = ob.worldToObject(p)
	return ob.bo.sdf(p)
}

func (ob *object) normalToWorld(v *tuple) *tuple {
	n := v.normalize()
	// If ob's transform is the identity matrix we can skip this
	if ob.hasTransform {
		n = ob.invTransformTransposed.mulByTuple(n)
	}
	n.w = 0.0
	if ob.parent != nil {
		n = ob.parent.normalToWorld(n.normalize())
	}
	return n.normalize()
}

type normalFuncHook = func(worldPoint, localPoint, worldNormal, localNormal *tuple) *tuple

func defaultNormalFuncHook(_, _, worldNormal, _ *tuple) *tuple {
	return worldNormal
}

func (ob *object) normalAt(worldPoint *tuple, hit *intersection) *tuple {
	localPoint := ob.worldToObject(worldPoint)
	localNormal := ob.bo.localNormalAt(localPoint, hit)
	worldNormal := ob.normalToWorld(localNormal)
	return ob.normalFuncHook(worldPoint, localPoint, worldNormal, localNormal)
}

func (ob *object) getMaterial() *material {
	return ob.material
}

func (ob *object) setMaterial(m *material) {
	ob.material = m
	if g, ok := ob.bo.(*group); ok {
		for _, o := range g.objects {
			o.setMaterial(m)
		}
	}
}

func (ob *object) intersects(r *ray) []*intersection {
	if ob.hasTransform {
		// TODO: Do this in place and just pass copies
		// into this func when we dont want this mutated
		r = r.transform(ob.invTransform)
	}
	return ob.bo.localIntersects(ob, r)
}

func (ob *object) getLines() []*line {
	lines := []*line{}
	for _, l := range ob.bo.getLines() {
		// object to world coords
		lines = append(lines, l.applyTransform(ob.transform))
	}
	return lines
}

func (ob *object) getTransform() matrix {
	return ob.transform
}

func (ob *object) setTransform(m matrix) {
	ob.transform = m
	ob.invTransform = m.inverse()
	ob.invTransformTransposed = ob.invTransform.transpose()
	ob.hasTransform = true
}

func (ob *object) equals(other *object) bool {
	return other.material.equals(ob.material) &&
		ob.transform.equals(other.transform) &&
		ob.bo.equals(other.bo)
}

func (ob *object) includes(other *object) bool {
	if g, ok := ob.bo.(*group); ok {
		return g.includes(other)
	}
	if c, ok := ob.bo.(*csg); ok {
		return c.includes(other)
	}
	return ob.equals(other)
}

// addObject is only applicable to groups
func (ob *object) addObject(o *object) {
	if g, ok := ob.bo.(*group); ok {
		g.addObject(ob, o)
	}
}

// addObjects is only applicable to groups
func (ob *object) addObjects(obs []*object) {
	if g, ok := ob.bo.(*group); ok {
		g.addObjects(ob, obs)
	}
}
