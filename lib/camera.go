package lib

import (
	"math"
	"math/rand"
	"sync"
)

type camera struct {
	hSize  int
	vSize  int
	fov    float64
	canvas *canvas

	transform    matrix
	invTransform matrix
	origin       *tuple

	pixelSize  float64
	height     float64
	width      float64
	halfWidth  float64
	halfHeight float64
}

func newCamera(hSize, vSize int, fov float64) *camera {
	halfView := math.Tan(fov / 2.0)
	aspect := float64(hSize) / float64(vSize)

	var halfWidth float64
	var halfHeight float64
	if aspect >= 1.0 {
		halfWidth = halfView
		halfHeight = halfView / aspect
	} else {
		halfWidth = halfView * aspect
		halfHeight = halfView
	}
	pixelSize := (halfWidth * 2.0) / float64(hSize)
	canvas := newCanvas(hSize, vSize)
	return &camera{
		hSize:        hSize,
		vSize:        vSize,
		fov:          fov,
		canvas:       canvas,
		transform:    defaultViewTransformation(),
		invTransform: defaultViewTransformation().inverse(),
		origin: defaultViewTransformation().inverse().
			mulByTuple(newPoint(0, 0, 0)),
		pixelSize:  pixelSize,
		width:      halfWidth * 2,
		height:     halfHeight * 2,
		halfWidth:  halfWidth,
		halfHeight: halfHeight,
	}
}

func (c *camera) setTransform(m matrix) {
	// TODO: Double check this works for the dir component
	c.transform = m
	c.invTransform = m.inverse()
	c.origin = c.invTransform.mulByTuple(newPoint(0, 0, 0))
}

func (c *camera) rayForPixel(px, py int) *ray {
	xOffset := (float64(px) + 0.5) * c.pixelSize
	yOffset := (float64(py) + 0.5) * c.pixelSize

	worldX := c.halfWidth - xOffset
	worldY := c.halfHeight - yOffset

	pixel := c.invTransform.mulByTuple(newPoint(worldX, worldY, -1))
	origin := c.origin
	direction := pixel.sub(origin).normalize()

	return newRay(origin, direction)
}

func (c *camera) render(w *world) *canvas {
	for y := 0; y < c.vSize; y++ {
		for x := 0; x < c.hSize; x++ {
			r := c.rayForPixel(x, y)
			col := w.colorAt(r, MAX_RECURSION_DEPTH)
			c.canvas.put(x, y, col)
		}
	}
	return c.canvas
}

func (c *camera) renderConcurrent(w *world) *canvas {
	var wg sync.WaitGroup
	for x := 0; x < c.hSize; x++ {
		wg.Add(1)
		go func(x int) {
			defer wg.Done()
			for y := 0; y < c.vSize; y++ {
				r := c.rayForPixel(x, y)
				col := w.colorAt(r, MAX_RECURSION_DEPTH)
				c.canvas.put(x, y, col)
			}
		}(x)
	}
	wg.Wait()
	return c.canvas
}

func (c *camera) renderConcurrentSuperSample(w *world, jitterRays int) *canvas {
	var wg sync.WaitGroup
	for x := 0; x < c.hSize; x++ {
		wg.Add(1)
		go func(x int) {
			defer wg.Done()
			for y := 0; y < c.vSize; y++ {
				r := c.rayForPixel(x, y)
				col := newColor(0, 0, 0) // w.colorAt(r, MAX_RECURSION_DEPTH)
				for i := 0; i < jitterRays; i++ {
					xJitter := (rand.Float64() - 0.5) * 0.001
					yJitter := (rand.Float64() - 0.5) * 0.001
					zJitter := (rand.Float64() - 0.5) * 0.001
					r = r.copy()
					r.direction.x = r.direction.x + r.direction.x*xJitter
					r.direction.y = r.direction.y + r.direction.y*yJitter
					r.direction.z = r.direction.z + r.direction.z*zJitter
					col = col.add(w.colorAt(r, MAX_RECURSION_DEPTH))
				}
				c.canvas.put(x, y, col.mul(1.0/float64(jitterRays)))
			}
		}(x)
	}
	wg.Wait()
	return c.canvas
}

func (c *camera) renderRasterise(w *world) *canvas {
	for _, o := range w.objects {
		for _, l := range o.getLines() {
			c.drawLine(c.canvas, l, newColor(0.5, 1, 0.5)) //o.getMaterial().color)
		}
	}
	return c.canvas
}

func (c *camera) drawLine(canvas *canvas, l *line, color *color) {
	cameraLine := l.applyTransform(c.transform)

	x1 := cameraLine.p1.x / -cameraLine.p1.z
	y1 := cameraLine.p1.y / -cameraLine.p1.z
	x2 := cameraLine.p2.x / -cameraLine.p2.z
	y2 := cameraLine.p2.y / -cameraLine.p2.z

	// normalise
	x1 = (x1 + c.halfWidth) / c.width
	y1 = (y1 + c.halfHeight) / c.height
	x2 = (x2 + c.halfWidth) / c.width
	y2 = (y2 + c.halfHeight) / c.height

	x1 = (1.0 - x1) * float64(c.hSize)
	y1 = (1.0 - y1) * float64(c.vSize)
	x2 = (1.0 - x2) * float64(c.hSize)
	y2 = (1.0 - y2) * float64(c.vSize)

	canvas.drawLine(int(x1), int(y1), int(x2), int(y2), color)
}
