package lib

import "math"

type pattern interface {
	getTransform() matrix
	setTransform(m matrix)
	colorAt(p *tuple) *color
}

type basePattern struct {
	color *color
}

func newBasePattern(color *color) *basePattern {
	return &basePattern{
		color: color,
	}
}

func (bp *basePattern) getTransform() matrix {
	return identity()
}

func (bp *basePattern) setTransform(_ matrix) {}

func (bp *basePattern) colorAt(p *tuple) *color {
	return bp.color
}

type stripedPattern struct {
	transform    matrix
	invTransform matrix
	colors       []pattern
}

func newStripedPattern(colors []*color) *stripedPattern {
	patterns := make([]pattern, len(colors))
	for i, c := range colors {
		patterns[i] = newBasePattern(c)
	}
	return &stripedPattern{
		transform:    identity(),
		invTransform: identity().inverse(),
		colors:       patterns,
	}
}

func newNestedStripedPattern(patterns []pattern) *stripedPattern {
	return &stripedPattern{
		transform:    identity(),
		invTransform: identity().inverse(),
		colors:       patterns,
	}
}

func (sp *stripedPattern) getTransform() matrix {
	return sp.transform
}

func (sp *stripedPattern) setTransform(m matrix) {
	sp.transform = m
	sp.invTransform = m.inverse()
}

func (sp *stripedPattern) colorAt(p *tuple) *color {
	patternPoint := sp.invTransform.mulByTuple(p)
	return sp.colors[int(math.Abs(patternPoint.x))%len(sp.colors)].colorAt(p)
}

type gradientPattern struct {
	transform    matrix
	invTransform matrix
	color1       *color
	color2       *color
}

func newGradientPattern(color1, color2 *color) *gradientPattern {
	return &gradientPattern{
		transform:    identity(),
		invTransform: identity().inverse(),
		color1:       color1,
		color2:       color2,
	}
}

func (gp *gradientPattern) getTransform() matrix {
	return gp.transform
}

func (gp *gradientPattern) setTransform(m matrix) {
	gp.transform = m
	gp.invTransform = m.inverse()
}

func (gp *gradientPattern) colorAt(p *tuple) *color {
	patternPoint := gp.invTransform.mulByTuple(p)
	dist := gp.color2.sub(gp.color1)
	fraction := patternPoint.x - math.Floor(patternPoint.x)
	return gp.color1.add(dist.mul(fraction))
}

type ringPattern struct {
	transform    matrix
	invTransform matrix
	colors       []pattern
}

func newRingPattern(colors []*color) *ringPattern {
	patterns := make([]pattern, len(colors))
	for i, c := range colors {
		patterns[i] = newBasePattern(c)
	}
	return &ringPattern{
		transform:    identity(),
		invTransform: identity().inverse(),
		colors:       patterns,
	}
}

func newNestedRingPattern(patterns []pattern) *ringPattern {
	return &ringPattern{
		transform:    identity(),
		invTransform: identity().inverse(),
		colors:       patterns,
	}
}

func (sp *ringPattern) getTransform() matrix {
	return sp.transform
}

func (sp *ringPattern) setTransform(m matrix) {
	sp.transform = m
	sp.invTransform = m.inverse()
}

func (sp *ringPattern) colorAt(p *tuple) *color {
	pp := sp.invTransform.mulByTuple(p)
	dist := math.Sqrt(pp.x*pp.x + pp.z*pp.z)
	index := int(dist) % len(sp.colors)
	return sp.colors[index].colorAt(p)
}

type checkerboardPattern struct {
	transform    matrix
	invTransform matrix
	color1       pattern
	color2       pattern
}

func newCheckerboardPattern(color1, color2 *color) *checkerboardPattern {
	return &checkerboardPattern{
		transform:    identity(),
		invTransform: identity().inverse(),
		color1:       newBasePattern(color1),
		color2:       newBasePattern(color2),
	}
}

func newNestedCheckerboardPattern(pattern1, pattern2 pattern) *checkerboardPattern {
	return &checkerboardPattern{
		transform:    identity(),
		invTransform: identity().inverse(),
		color1:       pattern1,
		color2:       pattern2,
	}
}

func (gp *checkerboardPattern) getTransform() matrix {
	return gp.transform
}

func (gp *checkerboardPattern) setTransform(m matrix) {
	gp.transform = m
	gp.invTransform = m.inverse()
}

func (gp *checkerboardPattern) colorAt(p *tuple) *color {
	pp := gp.invTransform.mulByTuple(p)
	if int(math.Floor(pp.x)+
		math.Floor(pp.y)+
		math.Floor(pp.z))%2 == 0 {
		return gp.color1.colorAt(pp)
	}
	return gp.color2.colorAt(pp)
}

type testPattern struct {
	transform    matrix
	invTransform matrix
}

func newTestPattern() *testPattern {
	return &testPattern{
		transform:    identity(),
		invTransform: identity().inverse(),
	}
}

func (tp *testPattern) getTransform() matrix {
	return tp.transform
}

func (tp *testPattern) setTransform(m matrix) {
	tp.transform = m
	tp.invTransform = m.inverse()
}

func (tp *testPattern) colorAt(p *tuple) *color {
	pp := tp.invTransform.mulByTuple(p)
	r := pp.x
	g := pp.y
	b := pp.z
	if r > 1.0 {
		r = 1.0
	}
	if g > 1.0 {
		g = 1.0
	}
	if b > 1.0 {
		b = 1.0
	}
	return newColor(pp.x, pp.y, pp.z)
}
