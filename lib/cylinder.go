package lib

import "math"

type cylinder struct {
	min      float64
	max      float64
	isClosed bool
}

func newCylinder(min, max float64) *object {
	o := newObject()
	o.bo = &cylinder{
		min: min,
		max: max,
	}
	return o
}

func newClosedCylinder(min, max float64) *object {
	o := newObject()
	o.bo = &cylinder{
		min:      min,
		max:      max,
		isClosed: true,
	}
	return o
}

func (cyl *cylinder) sdf(p *tuple) float64 {
	panic(1)
}

func (cyl *cylinder) localIntersects(ob *object, r *ray) []*intersection {
	a := r.direction.x*r.direction.x + r.direction.z*r.direction.z
	if isClose(a, EPSILON) {
		return cyl.intersectsCaps(r, ob, []*intersection{})
	}
	b := 2*r.origin.x*r.direction.x +
		2*r.origin.z*r.direction.z
	c := r.origin.x*r.origin.x +
		r.origin.z*r.origin.z - 1.0
	disc := b*b - 4*a*c
	if disc < 0.0 {
		return []*intersection{}
	}
	t0 := (-b - math.Sqrt(disc)) / (2 * a)
	t1 := (-b + math.Sqrt(disc)) / (2 * a)

	if t0 > t1 {
		t0, t1 = t1, t0
	}

	xs := []*intersection{}
	y0 := r.origin.y + t0*r.direction.y
	if cyl.min < y0 && y0 < cyl.max {
		xs = []*intersection{newIntersection(t0, ob)}
	}
	y1 := r.origin.y + t1*r.direction.y
	if cyl.min < y1 && y1 < cyl.max {
		xs = append(xs, newIntersection(t1, ob))
	}
	return cyl.intersectsCaps(r, ob, xs)
}

func (c *cylinder) checkCap(t float64, r *ray) bool {
	x := r.origin.x + t*r.direction.x
	z := r.origin.z + t*r.direction.z
	return (x*x + z*z) <= 1.0
}

func (c *cylinder) intersectsCaps(r *ray, ob *object, xs []*intersection) []*intersection {
	if !c.isClosed || isClose(r.direction.y, EPSILON) {
		return xs
	}
	t := (c.min - r.origin.y) / r.direction.y
	if c.checkCap(t, r) {
		xs = insertIntersection(newIntersection(t, ob), xs)
	}
	t = (c.max - r.origin.y) / r.direction.y
	if c.checkCap(t, r) {
		xs = insertIntersection(newIntersection(t, ob), xs)
	}
	return xs
}

func (c *cylinder) localNormalAt(p *tuple, _ *intersection) *tuple {
	dist := p.x*p.x + p.z*p.z
	if dist < 1.0 && p.y >= (c.max-EPSILON) {
		return newVector(0, 1, 0)
	}
	if dist < 1.0 && p.y <= (c.min+EPSILON) {
		return newVector(0, -1, 0)
	}
	return newVector(p.x, 0, p.z)
}

func (c *cylinder) equals(other baseObject) bool {
	_, ok := other.(*cylinder)
	return ok
}

func (c *cylinder) bounds() (*tuple, *tuple) {
	minY := -INFINITY
	maxY := INFINITY
	if c.isClosed {
		minY = c.min
		maxY = c.max
	}
	min := newPoint(-1, minY, -1)
	max := newPoint(1, maxY, 1)
	return min, max
}

func (c *cylinder) getLines() []*line {
	return nil
}
