package lib

import (
	"math"
	"testing"
)

func TestLighting(t *testing.T) {
	m := newSphere()
	position := newPoint(0, 0, 0)

	eyeV := newVector(0, 0, -1)
	normalV := newVector(0, 0, -1)
	light := newPointLight(newPoint(0, 0, -10), newColor(1, 1, 1))
	result := lighting(m, light, position, eyeV, normalV, false)
	if !result.equals(newColor(1.9, 1.9, 1.9)) {
		t.Error("incorrect result for lighting")
		t.Error(result.r, result.g, result.b)
	}

	r2 := math.Sqrt(2) / 2
	eyeV = newVector(0, r2/2.0, r2/2.0)
	normalV = newVector(0, 0, -1)
	light = newPointLight(newPoint(0, 0, -10), newColor(1, 1, 1))
	result = lighting(m, light, position, eyeV, normalV, false)
	if !result.equals(newColor(1, 1, 1)) {
		t.Error("incorrect result for lighting")
		t.Error(result.r, result.g, result.b)
	}

	eyeV = newVector(0, 0, -1)
	normalV = newVector(0, 0, -1)
	light = newPointLight(newPoint(0, 10, -10), newColor(1, 1, 1))
	result = lighting(m, light, position, eyeV, normalV, false)
	if !result.equals(newColor(0.7363961030678927,
		0.7363961030678927, 0.7363961030678927)) {
		t.Error("incorrect result for lighting")
		t.Error(result.r, result.g, result.b)
	}

	eyeV = newVector(0, 0, -1)
	normalV = newVector(0, 0, -1)
	light = newPointLight(newPoint(0, 0, 10), newColor(1, 1, 1))
	result = lighting(m, light, position, eyeV, normalV, false)
	if !result.equals(newColor(0.1, 0.1, 0.1)) {
		t.Error("incorrect result for lighting")
		t.Error(result.r, result.g, result.b)
	}
}
