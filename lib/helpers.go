package lib

import "math"

func isClose(a, b float64) bool {
	return math.Abs(a-b) < EPSILON
}

func isThisClose(a, b, epsilon float64) bool {
	return math.Abs(a-b) < epsilon
}

func mix(x, y, a float64) float64 {
	return x*(1.0-a) + y*a
}

func clamp(x, min, max float64) float64 {
	return math.Min(math.Max(x, min), max)
}

// quadratic smin
func smin(a, b, k float64) float64 {
	k *= 4.0
	h := math.Max(k-math.Abs(a-b), 0.0) / k
	return math.Min(a, b) - h*h*k*(1.0/4.0)
}
