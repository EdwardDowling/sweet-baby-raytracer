package lib

import "math"

type plane struct{}

func newPlane() *object {
	o := newObject()
	o.bo = &plane{}
	return o
}

func (p *plane) localNormalAt(_ *tuple, _ *intersection) *tuple {
	return newVector(0, 1, 0)
}

func (p *plane) localIntersects(ob *object, r *ray) []*intersection {
	if math.Abs(r.direction.y) < EPSILON {
		return []*intersection{}
	}
	return []*intersection{
		newIntersection(-r.origin.y/r.direction.y, ob),
	}
}

func (p *plane) sdf(_ *tuple) float64 {
	panic(1)
}

func (p *plane) equals(other baseObject) bool {
	_, ok := other.(*plane)
	return ok
}
func (p *plane) bounds() (*tuple, *tuple) {
	min := newPoint(-INFINITY, -EPSILON, -INFINITY)
	max := newPoint(INFINITY, EPSILON, INFINITY)
	return min, max
}

func (p *plane) getLines() []*line {
	return nil
}
