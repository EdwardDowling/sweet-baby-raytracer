package lib

import (
	"math"
)

const (
	GROUP_LIMIT = 16
)

// TODO: Make material properties work for groups
type group struct {
	objects []*object

	lowerBounds *tuple
	upperBounds *tuple
}

func newGroup() *object {
	o := newObject()
	o.bo = &group{}
	return o
}

func newGroupFromObjects(obs []*object) *object {
	o := newGroup()
	o.addObjects(obs)
	return o
}

// localNormalAt is undefined for groups
func (g *group) localNormalAt(_ *tuple, _ *intersection) *tuple {
	panic(1)
}

func (g *group) sdf(p *tuple) float64 {
	panic(1)
}

func (g *group) localIntersects(o *object, r *ray) []*intersection {
	xs := []*intersection{}
	if !g.intersectsBounds(o, r) {
		return xs
	}
	for _, o := range g.objects {
		localXs := o.intersects(r) // THIS RAY SHOULDNT BE MUTATED
		for _, x := range localXs {
			xs = insertIntersection(x, xs)
		}
	}
	return xs
}

func (g *group) equals(o baseObject) bool {
	return false
}

func (g *group) addObject(outer, other *object) {
	g.objects = append(g.objects, other)
	other.parent = outer
	lower, upper := g.bounds()
	g.lowerBounds = lower
	g.upperBounds = upper
}

func (g *group) addObjects(outer *object, others []*object) {
	g.objects = append(g.objects, others...)
	for _, other := range others {
		other.parent = outer
	}
	lower, upper := g.bounds()
	g.lowerBounds = lower
	g.upperBounds = upper
	if len(g.objects) > GROUP_LIMIT {
		g.partition(outer)
	}
}

func (g *group) bounds() (*tuple, *tuple) {
	minX := -INFINITY
	minY := -INFINITY
	minZ := -INFINITY

	maxX := INFINITY
	maxY := INFINITY
	maxZ := INFINITY

	for _, child := range g.objects {
		childMin, childMax := child.bo.bounds()
		localMin := child.getTransform().mulByTuple(childMin)
		localMax := child.getTransform().mulByTuple(childMax)

		minX = math.Min(minX, localMin.x)
		minY = math.Min(minY, localMin.y)
		minZ = math.Min(minZ, localMin.z)

		maxX = math.Max(maxX, localMax.x)
		maxY = math.Max(maxY, localMax.y)
		maxZ = math.Max(maxZ, localMax.z)
	}
	return newPoint(minX, minY, minZ), newPoint(maxX, maxY, maxZ)
}

func (g *group) intersectsBounds(_ *object, r *ray) bool {
	minBounds := g.lowerBounds
	maxBounds := g.upperBounds

	xTMin, xTMax := g.checkAxis(r.origin.x, r.direction.x, minBounds.x, maxBounds.x)
	yTMin, yTMax := g.checkAxis(r.origin.y, r.direction.y, minBounds.y, maxBounds.y)
	zTMin, zTMax := g.checkAxis(r.origin.z, r.direction.z, minBounds.z, maxBounds.z)

	tMin := math.Max(xTMin, math.Max(yTMin, zTMin))
	tMax := math.Min(xTMax, math.Min(yTMax, zTMax))

	if tMin > tMax || tMax < 0.0 {
		return false
	}
	return true
}

func (g *group) checkAxis(origin, direction, min, max float64) (float64, float64) {
	// Offset plane intersection
	tMinNumerator := (min - origin)
	tMaxNumerator := (max - origin)
	var tMin float64
	var tMax float64
	if math.Abs(direction) >= EPSILON {
		tMin = tMinNumerator / direction
		tMax = tMaxNumerator / direction
	} else {
		tMin = tMinNumerator * INFINITY
		tMax = tMaxNumerator * INFINITY
	}
	if tMin > tMax {
		tMin, tMax = tMax, tMin
	}
	return tMin, tMax
}

func (g *group) partition(outer *object) {
	objects := []*object{}
	lower := []*object{}
	upper := []*object{}

	lowerGroup, upperGroup := g.split()
	for _, o := range g.objects {
		if lowerGroup.contains(o) {
			lower = append(lower, o)
		} else if upperGroup.contains(o) {
			upper = append(upper, o)
		} else {
			objects = append(objects, o)
		}
	}
	if len(g.objects) == len(lower) || len(g.objects) == len(upper) {
		return
	}
	g.objects = objects
	if len(lower) != 0 {
		lowerObject := newObject()
		lowerGroup.objects = lower
		for _, o := range lowerGroup.objects {
			o.parent = lowerObject
		}
		lowerObject.bo = lowerGroup
		lowerObject.parent = outer
		if len(lowerGroup.objects) > GROUP_LIMIT {
			lowerGroup.partition(lowerObject)
		}
		g.objects = append(g.objects, lowerObject)
	}
	if len(upper) != 0 {
		upperObject := newObject()
		upperGroup.objects = upper
		for _, o := range upperGroup.objects {
			o.parent = upperObject
		}
		upperObject.bo = upperGroup
		upperObject.parent = outer
		if len(upperGroup.objects) > GROUP_LIMIT {
			upperGroup.partition(upperObject)
		}
		g.objects = append(g.objects, upperObject)
	}
}

func (g *group) containsPoint(p *tuple) bool {
	return g.lowerBounds.x <= p.x &&
		g.lowerBounds.y <= p.y &&
		g.lowerBounds.z <= p.z &&
		p.x <= g.upperBounds.x &&
		p.y <= g.upperBounds.y &&
		p.z <= g.upperBounds.z
}

func (g *group) contains(other *object) bool {
	lowerBounds, upperBounds := other.bo.bounds()
	return g.containsPoint(lowerBounds) &&
		g.containsPoint(upperBounds)
}

func (g *group) includes(other *object) bool {
	if g.equals(other.bo) {
		return true
	}
	for _, o := range g.objects {
		if o.includes(other) {
			return true
		}
	}
	return false
}

// TODO: Come up with better way to evenly split group
func (g *group) split() (*group, *group) {
	dx := g.upperBounds.x - g.lowerBounds.x
	dy := g.upperBounds.y - g.lowerBounds.y
	dz := g.upperBounds.z - g.lowerBounds.z

	greatestD := math.Max(dx, math.Max(dy, dz))

	x0 := g.lowerBounds.x
	y0 := g.lowerBounds.y
	z0 := g.lowerBounds.z
	x1 := g.upperBounds.x
	y1 := g.upperBounds.y
	z1 := g.upperBounds.z

	if isClose(greatestD, dx) {
		x0 += dx / 2.0
		x1 = x0
	} else if isClose(greatestD, dy) {
		y0 += dy / 2.0
		y1 = y0
	} else {
		z0 += dz / 2.0
		z1 = z0
	}

	minMid := newPoint(x0, y0, z0)
	maxMid := newPoint(x1, y1, z1)

	lower := &group{
		lowerBounds: g.lowerBounds,
		upperBounds: maxMid,
	}
	upper := &group{
		lowerBounds: minMid,
		upperBounds: g.upperBounds,
	}

	return lower, upper
}

func (g *group) getLines() []*line {
	lines := []*line{}
	for _, o := range g.objects {
		lines = append(lines, o.getLines()...)
	}
	return lines
}
