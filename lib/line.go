package lib

type line struct {
	p1 *tuple
	p2 *tuple
}

func (l *line) applyTransform(m matrix) *line {
	return &line{
		p1: m.mulByTuple(l.p1),
		p2: m.mulByTuple(l.p2),
	}
}
