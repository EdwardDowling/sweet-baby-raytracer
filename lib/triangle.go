package lib

import (
	"math"
)

type triangle struct {
	p1, p2, p3 *tuple
	e1         *tuple // p1 -> p2
	e2         *tuple // p3 -> p1
	normal     *tuple

	// Normals at vertices used for smooth triangles
	n1, n2, n3 *tuple
}

func newTriangle(p1, p2, p3 *tuple) *object {
	o := newObject()
	e1 := p2.sub(p1)
	e2 := p3.sub(p1)
	normal := e2.cross(e1).normalize()
	o.bo = &triangle{
		p1:     p1,
		p2:     p2,
		p3:     p3,
		e1:     e1,
		e2:     e2,
		normal: normal,
	}
	return o
}

func newSmoothTriangle(p1, p2, p3, n1, n2, n3 *tuple) *object {
	o := newObject()
	e1 := p2.sub(p1)
	e2 := p3.sub(p1)
	normal := e2.cross(e1).normalize()
	o.bo = &triangle{
		p1:     p1,
		p2:     p2,
		p3:     p3,
		e1:     e1,
		e2:     e2,
		normal: normal,
		n1:     n1,
		n2:     n2,
		n3:     n3,
	}
	return o
}

func (t *triangle) localIntersects(ob *object, r *ray) []*intersection {
	// Möller–Trumbore
	dirCrossE2 := r.direction.cross(t.e2)
	det := t.e1.dot(dirCrossE2)
	if math.Abs(det) < EPSILON {
		return []*intersection{}
	}
	f := 1.0 / det
	p1ToOrigin := r.origin.sub(t.p1)
	u := f * p1ToOrigin.dot(dirCrossE2)
	if u < 0.0 || u > 1.0 {
		return []*intersection{}
	}
	originCrossE1 := p1ToOrigin.cross(t.e1)
	v := f * r.direction.dot(originCrossE1)
	if v < 0.0 || u+v > 1.0 {
		return []*intersection{}
	}
	tOffset := f * t.e2.dot(originCrossE1)
	// TODO: Unjank this
	if t.n1 != nil {
		return []*intersection{newIntersectionWithUV(tOffset, ob, u, v)}
	}
	return []*intersection{newIntersection(tOffset, ob)}
}

func (t *triangle) localNormalAt(p *tuple, hit *intersection) *tuple {
	// TODO: Panic if any vertex normal isn't set unless none are set.
	// Or maybe split smooth triangles off into a seperate type.
	if t.n1 != nil {
		// Interpolate normals for smooth triangles
		return t.n2.mul(hit.u).
			add(t.n3.mul(hit.v).
				add(t.n1.mul(1.0 - hit.u - hit.v))).normalize()

	}
	return t.normal
}

func (t *triangle) sdf(p *tuple) float64 {
	panic(1)
}

func (t *triangle) equals(other baseObject) bool {
	_, ok := other.(*triangle)
	return ok
}

func (t *triangle) bounds() (*tuple, *tuple) {
	minX := math.Min(t.p1.x, math.Min(t.p2.x, t.p3.x))
	minY := math.Min(t.p1.y, math.Min(t.p2.y, t.p3.y))
	minZ := math.Min(t.p1.z, math.Min(t.p2.z, t.p3.z))

	maxX := math.Max(t.p1.x, math.Max(t.p2.x, t.p3.x))
	maxY := math.Max(t.p1.y, math.Max(t.p2.y, t.p3.y))
	maxZ := math.Max(t.p1.z, math.Max(t.p2.z, t.p3.z))
	return newPoint(minX, minY, minZ), newPoint(maxX, maxY, maxZ)
}

func (t *triangle) getLines() []*line {
	return []*line{
		{t.p1, t.p2},
		{t.p2, t.p3},
		{t.p3, t.p1},
	}
}
