package lib

type ray struct {
	origin    *tuple
	direction *tuple
}

func newRay(origin, direction *tuple) *ray {
	return &ray{
		origin:    origin,
		direction: direction,
	}
}

func (r *ray) position(t float64) *tuple {
	return r.origin.add(r.direction.mul(t))
}

func (r *ray) transform(m matrix) *ray {
	return &ray{
		origin:    m.mulByTuple(r.origin),
		direction: m.mulByTuple(r.direction),
	}
}

func (r *ray) equals(o *ray) bool {
	return r.origin.equals(o.origin) &&
		r.direction.equals(o.direction)
}

func (r *ray) copy() *ray {
	return newRay(r.origin.copy(), r.direction.copy())
}

type intersection struct {
	// Multiple of direction vector from origin along
	// ray at which intersection occured.
	t float64
	o *object
	// Position on triangles intersection occured
	// 0 -> 1
	u, v float64
}

func (i *intersection) equals(o *intersection) bool {
	return isClose(i.t, o.t) && i.o == o.o
}

func newIntersection(t float64, o *object) *intersection {
	return &intersection{
		t: t,
		o: o,
	}
}

func newIntersectionWithUV(t float64, o *object, u, v float64) *intersection {
	return &intersection{
		t: t,
		o: o,
		u: u,
		v: v,
	}
}

func hit(intersections []*intersection) *intersection {
	if len(intersections) == 0 {
		return nil
	}
	var ret *intersection
	for _, i := range intersections {
		if i.t < 0 {
			continue
		}
		if ret == nil || ret.t > i.t {
			ret = i
		}
	}
	return ret
}

func intersectionsContainObject(xs []*intersection, o *object) (int, bool) {
	for index, currX := range xs {
		if o.equals(currX.o) {
			return index, true
		}
	}
	return -1, false
}

func intersectionsRemove(xs []*intersection, index int) []*intersection {
	if index == len(xs)-1 {
		return xs[:index]
	}
	return append(xs[:index], xs[index+1:]...)
}
