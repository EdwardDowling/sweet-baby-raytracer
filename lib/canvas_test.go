package lib

import (
	"bytes"
	"testing"
)

func TestCanvas(t *testing.T) {
	c := newCanvas(12, 42)
	if c.width != 12 && c.height != 42 {
		t.Error("unexpected width or height when creating new canvas")
	}
	black := newColor(0, 0, 0)
	for _, row := range c.pixels {
		for _, pixel := range row {
			if !pixel.equals(black) {
				t.Error("canvas pixel not black after creation")
				return
			}
		}
	}
}

func TestCanvasPutAndGet(t *testing.T) {
	c := newCanvas(10, 20)
	c.put(2, 3, newColor(1, 0, 0))
	if !c.get(2, 3).equals(newColor(1, 0, 0)) {
		t.Error("incorrect result for canvas put and get")
	}
}

func TestCanvasToPPM(t *testing.T) {
	c := newCanvas(5, 3)
	c.put(0, 0, newColor(1.0/255, 2.0/255, 3.0/255))
	var buf bytes.Buffer
	if err := c.toPPM(&buf); err != nil {
		t.Error(err)
	}
	got := buf.String()
	want :=
		`P3
5 3
255
1 2 3 0 0 0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
`
	if got != want {
		t.Error("incorrect PPM for canvas to PPM")
		t.Error(got)
		t.Error(want)
	}

	c = newCanvas(70, 1)
	var buf2 bytes.Buffer
	if err := c.toPPM(&buf2); err != nil {
		t.Error(err)
	}
	got = buf2.String()
	want =
		`P3
70 1
255
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
`
	if got != want {
		t.Error("incorrect PPM for canvas to PPM")
		t.Error(got)
		t.Error(want)
	}
}
