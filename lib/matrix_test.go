package lib

import (
	"math"
	"testing"
)

func TestInit(t *testing.T) {
	m := matrix{
		{-3, 5},
		{1, -2},
	}
	for _, tc := range []struct {
		x    int
		y    int
		want float64
	}{
		{
			x:    0,
			y:    0,
			want: -3,
		},
		{
			x:    0,
			y:    1,
			want: 5,
		},
		{
			x:    1,
			y:    0,
			want: 1,
		},
		{
			x:    1,
			y:    1,
			want: -2,
		},
	} {
		if !isClose(m[tc.x][tc.y], tc.want) {
			t.Errorf("unexpected orientation on init matrix: got %f want %f",
				m[tc.x][tc.y], tc.want)
		}
	}
}

func TestEquailty(t *testing.T) {
	m1 := matrix{
		{1, 2, 3, 4},
		{5, 6, 7, 8},
		{9, 8, 7, 6},
		{5, 4, 3, 2},
	}
	m2 := matrix{
		{1, 2, 3, 4},
		{5, 6, 7, 8},
		{9, 8, 7, 6},
		{5, 4, 3, 2},
	}
	m3 := matrix{
		{1, 2, 3, 4},
		{5, 6, 7, 8},
		{9, 8, -7, 6},
		{5, 4, 3, 2},
	}
	if !m1.equals(m2) {
		t.Error("unexpected result from matrix equality")
	}
	if m1.equals(m3) {
		t.Error("unexpected result from matrix equality")
	}
}

func TestMul(t *testing.T) {
	m1 := matrix{
		{1, 2, 3, 4},
		{5, 6, 7, 8},
		{9, 8, 7, 6},
		{5, 4, 3, 2},
	}
	m2 := matrix{
		{-2, 1, 2, 3},
		{3, 2, 1, -1},
		{4, 3, 6, 5},
		{1, 2, 7, 8},
	}
	want := matrix{
		{20, 22, 50, 48},
		{44, 54, 114, 108},
		{40, 58, 110, 102},
		{16, 26, 46, 42},
	}
	if !want.equals(m1.mul(m2)) {
		t.Error("unexpected result from matrix multiplication")
	}
	if !m1.equals(m1.mul(identity())) {
		t.Error("unexpected result from identity matrix multiplication")
	}
	if !m2.equals(m2.mul(identity())) {
		t.Error("unexpected result from identity matrix multiplication")
	}
}

func TestMulByTuple(t *testing.T) {
	m := matrix{
		{1, 2, 3, 4},
		{2, 4, 4, 2},
		{8, 6, 4, 1},
		{0, 0, 0, 1},
	}
	tp := newTuple(1, 2, 3, 1)
	want := newTuple(18, 24, 33, 1)
	if !want.equals(m.mulByTuple(tp)) {
		t.Error("unexpected result from matrix multiplication by tuple")
	}

	if !tp.equals(identity().mulByTuple(tp)) {
		t.Error("unexpected result from identity matrix multiplication by tuple")
	}
}

func TestTranspose(t *testing.T) {
	m := matrix{
		{0, 9, 3, 0},
		{9, 8, 0, 8},
		{1, 8, 5, 3},
		{0, 0, 5, 8},
	}
	want := matrix{
		{0, 9, 1, 0},
		{9, 8, 8, 0},
		{3, 0, 5, 5},
		{0, 8, 3, 8},
	}
	if !m.transpose().equals(want) {
		t.Error("unexpected value from matrix transpose")
	}
}

func TestSubMatrix(t *testing.T) {
	m := matrix{
		{1, 5, 0},
		{-3, 2, 7},
		{0, 6, -3},
	}
	want := matrix{
		{-3, 2},
		{0, 6},
	}
	if !want.equals(m.subMatrix(0, 2)) {
		t.Error("unexpected result getting submatrix")
	}
	m = matrix{
		{-6, 1, 1, 6},
		{-8, 5, 8, 6},
		{-1, 0, 8, 2},
		{-7, 1, -1, 1},
	}
	want = matrix{
		{-6, 1, 6},
		{-8, 8, 6},
		{-7, -1, 1},
	}
	if !want.equals(m.subMatrix(2, 1)) {
		t.Error("unexpected result getting submatrix")
	}
}

func TestCofactor(t *testing.T) {
	m := matrix{
		{3, 5, 0},
		{2, -1, -7},
		{6, -1, 5},
	}
	if !isClose(m.cofactor(0, 0), -12) {
		t.Error("unexpected result for matrix cofactor")
	}
	if !isClose(m.cofactor(1, 0), -25) {
		t.Error("unexpected result for matrix cofactor")
	}

	m = matrix{
		{-2, -8, 3, 5},
		{-3, 1, 7, 3},
		{1, 2, -9, 6},
		{-6, 7, 7, -9},
	}

	if !isClose(m.cofactor(0, 0), 690) {
		t.Error("unexpected result for matrix cofactor")
	}
	if !isClose(m.cofactor(0, 3), 51) {
		t.Error("unexpected result for matrix cofactor")
	}
}

func TestDeterminant(t *testing.T) {
	m := matrix{
		{1, 5},
		{-3, 2},
	}
	if !isClose(m.determinant(), 17) {
		t.Error("unexpected result for determinant")
	}
	m = matrix{
		{1, 2, 6},
		{-5, 8, -4},
		{2, 6, 4},
	}

	if !isClose(m.determinant(), -196) {
		t.Error("unexpected result for determinant")
	}
	m = matrix{
		{-2, -8, 3, 5},
		{-3, 1, 7, 3},
		{1, 2, -9, 6},
		{-6, 7, 7, -9},
	}

	if !isClose(m.determinant(), -4071) {
		t.Error("unexpected result for determinant")
	}
}

func TestInverse(t *testing.T) {
	m := matrix{
		{-5, 2, 6, -8},
		{1, -5, 1, 8},
		{7, 7, -6, -7},
		{1, -3, 7, 4},
	}
	if !isClose(m.determinant(), 532) {
		t.Error("unexpected result for determinant")
	}
	if !isClose(m.cofactor(3, 2), 105) {
		t.Error("unexpected result for cofactor")
	}
	inverseM := m.inverse()
	if !isClose(inverseM[3][2], -160.0/532.0) {
		t.Error("unexpected result for inverse")
	}

	a := m
	b := matrix{
		{-2, -8, 3, 5},
		{-3, 1, 7, 3},
		{1, 2, -9, 6},
		{-6, 7, 7, -9},
	}
	c := a.mul(b)
	if !a.equals(c.mul(b.inverse())) {
		t.Error("unexpected result for inverse")
	}
}

func TestTranslation(t *testing.T) {
	transform := translation(5, -3, 2)
	tp := newPoint(-3, 4, 5)
	if !transform.mulByTuple(tp).equals(newPoint(2, 1, 7)) {
		t.Error("unexpected result for tranlation")
	}
	if !transform.inverse().mulByTuple(tp).equals(newPoint(-8, 7, 3)) {
		t.Error("unexpected result for tranlation")
	}

	v := newVector(-3, 4, 5)
	if !transform.mulByTuple(v).equals(v) {
		t.Error("unexpected result for tranlation")
	}
}

func TestScaling(t *testing.T) {
	transform := scaling(2, 3, 4)
	p := newPoint(-4, 6, 8)
	if !transform.mulByTuple(p).equals(newPoint(-8, 18, 32)) {
		t.Error("unexpected result for scaling")
	}
	v := newVector(-4, 6, 8)
	if !transform.mulByTuple(v).equals(newVector(-8, 18, 32)) {
		t.Error("unexpected result for scaling")
	}

	if !transform.inverse().mulByTuple(v).equals(newVector(-2, 2, 2)) {
		t.Error("unexpected result for scaling")
	}
}

func TestTransformations(t *testing.T) {
	P := newPoint(1, 0, 1)
	A := rotationX(math.Pi / 2.0)
	B := scaling(5, 5, 5)
	C := translation(10, 5, 7)

	if !A.mulByTuple(P).equals(newPoint(1, -1, 0)) {
		t.Error("unexpected result for rotation")
	}
	P = A.mulByTuple(P)
	if !B.mulByTuple(P).equals(newPoint(5, -5, 0)) {
		t.Error("unexpected result for scaling")
	}
	P = B.mulByTuple(P)
	if !C.mulByTuple(P).equals(newPoint(15, 0, 7)) {
		t.Error("unexpected result for translation")
	}

	T := identity().
		rotateX(math.Pi/2.0).
		scale(5, 5, 5).
		translate(10, 5, 7)
	result := T.mulByTuple(newPoint(1, 0, 1))
	if !result.equals(newPoint(15, 0, 7)) {
		t.Error("unexpected result for chained transformations")
	}
}
