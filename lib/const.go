package lib

import "math"

const (
	EPSILON             = 0.000000001
	INFINITY            = math.MaxFloat64
	MAX_RECURSION_DEPTH = 8
	MAX_MARCHING_STEPS  = 100000
	MAX_MARCHIN_DIST    = 1000
)
