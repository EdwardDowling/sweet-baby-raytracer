package lib

const (
	REFRACTIVE_INDEX_VACUUM  = 1.0
	REFRACTIVE_INDEX_AIR     = 1.00029
	REFRACTIVE_INDEX_WATER   = 1.333
	REFRACTIVE_INDEX_GLASS   = 1.52
	REFRACTIVE_INDEX_DIAMOND = 2.417
)

type material struct {
	color   *color
	pattern pattern

	ambient    float64
	diffuse    float64
	specular   float64
	shininess  float64
	reflective float64

	transparency    float64
	refractiveIndex float64

	noShadow bool
}

func newMaterial(color *color,
	ambient float64, diffuse float64,
	specular float64, shininess float64) *material {
	return &material{
		color:     color,
		ambient:   ambient,
		diffuse:   diffuse,
		specular:  specular,
		shininess: shininess,
	}
}

func defaultMaterial() *material {
	return &material{
		color:           newColor(1, 1, 1),
		ambient:         0.1,
		diffuse:         0.9,
		specular:        0.9,
		shininess:       200.0,
		reflective:      0.0,
		transparency:    0.0,
		refractiveIndex: 1.0,
	}
}

func defaultMaterialWithColor(r, g, b float64) *material {
	return &material{
		color:           newColor(r, g, b),
		ambient:         0.1,
		diffuse:         0.9,
		specular:        0.9,
		shininess:       200.0,
		reflective:      0.0,
		transparency:    0.0,
		refractiveIndex: 1.0,
	}
}

func glassMaterial() *material {
	return &material{
		color:           newColor(0.5, 0.5, 0.5),
		ambient:         0.05,
		diffuse:         0.1,
		specular:        0.9,
		shininess:       300.0,
		reflective:      0.7,
		transparency:    0.7,
		refractiveIndex: REFRACTIVE_INDEX_GLASS,
		noShadow:        true,
	}
}

func mirrorMaterial() *material {
	return &material{
		color:           newColor(0.2, 0.2, 0.2),
		ambient:         0.00,
		diffuse:         0.1,
		specular:        1,
		shininess:       300.0,
		reflective:      0.9,
		transparency:    0.0,
		refractiveIndex: 0.0,
		noShadow:        false,
	}
}

func (m *material) equals(o *material) bool {
	return m.color.equals(o.color) &&
		isClose(m.ambient, o.ambient) &&
		isClose(m.diffuse, o.diffuse) &&
		isClose(m.specular, o.specular) &&
		isClose(m.shininess, o.shininess)
}
