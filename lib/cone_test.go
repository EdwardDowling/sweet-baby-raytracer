package lib

import "testing"

func TestConeBasicIntersections(t *testing.T) {
	s := newCone(-100, 100)
	for index, tc := range []struct {
		origin *tuple
		dir    *tuple
		t0     float64
		t1     float64
	}{
		{
			origin: newPoint(0, 0, -5),
			dir:    newVector(0, 0, 1),
			t0:     5,
			t1:     5,
		},
		{
			origin: newPoint(0, 0, -5),
			dir:    newVector(1, 1, 1),
			t0:     8.660254,
			t1:     8.660254,
		},
		{
			origin: newPoint(1, 1, -5),
			dir:    newVector(-0.5, -1, 1),
			t0:     4.550056,
			t1:     49.449944,
		},
	} {
		dir := tc.dir.normalize()
		r := newRay(tc.origin, dir)
		xs := s.bo.localIntersects(s, r)
		if len(xs) != 2 {
			t.Error("incorrect number of intersections")
			continue
		}
		if !isClose(tc.t0, xs[0].t) {
			t.Errorf("incorrect fist intersection: %d", index)
			t.Errorf("want: %f, got: %f", tc.t0, xs[0].t)
		}
		if !isClose(tc.t1, xs[1].t) {
			t.Errorf("incorrect second intersection: %d", index)
			t.Errorf("want: %f, got: %f", tc.t1, xs[1].t)
		}
	}
}

// func TestConeParallel(t *testing.T) {
// 	s := newCone(-1, 1)
// 	dir := newVector(0, 1, 1).normalize()
// 	r := newRay(newPoint(0, 0, -1), dir)
// 	xs := s.bo.localIntersects(s, r)
// 	if len(xs) != 1 {
// 		t.Errorf("incorrect number of intersections: got %d", len(xs))
// 		return
// 	}
// 	if !isClose(xs[0].t, 0.35355) {
// 		t.Errorf("got: %f", xs[0].t)
// 	}
// }
