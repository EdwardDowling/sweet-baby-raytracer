package lib

import (
	"math"
)

type world struct {
	lights  []*light
	objects []*object
}

func defaultWorld() *world {
	s1 := newSphere()
	s1.material.color = newColor(0.8, 1.0, 0.6)
	s1.material.diffuse = 0.7
	s1.material.specular = 0.2

	s2 := newSphere()
	s2.setTransform(scaling(0.5, 0.5, 0.5))

	return &world{
		lights: []*light{
			newPointLight(newPoint(-10, 10, -10), newColor(1, 1, 1)),
		},
		objects: []*object{s1, s2},
	}
}

func newViewTransformation(from, to, up *tuple) matrix {
	forward := to.sub(from).normalize()
	upN := up.normalize()
	left := forward.cross(upN)
	trueUp := left.cross(forward)
	orientation := matrix{
		{left.x, left.y, left.z, 0},
		{trueUp.x, trueUp.y, trueUp.z, 0},
		{-forward.x, -forward.y, -forward.z, 0},
		{0, 0, 0, 1},
	}
	return orientation.mul(translation(-from.x, -from.y, -from.z))
}

func defaultViewTransformation() matrix {
	return newViewTransformation(newPoint(0, 0, 0),
		newPoint(0, 0, -1), newVector(0, 1, 0))
}

func (w *world) groupObjects() {
	g := newGroup()
	g.addObjects(w.objects)
	w.objects = []*object{g}
}

func (w *world) contains(o *object) bool {
	for _, obj := range w.objects {
		if obj.equals(o) {
			return true
		}
	}
	return false
}

func (w *world) intersects(r *ray) []*intersection {
	worldXs := []*intersection{}
	for _, o := range w.objects {
		xs := o.intersects(r)
		for index := 0; index < len(xs); index++ {
			worldXs = insertIntersection(xs[index], worldXs)
		}
	}
	return worldXs
}

func insertIntersection(x *intersection, xs []*intersection) []*intersection {
	if len(xs) == 0 {
		return []*intersection{x}
	}

	for i := range xs {
		if x.t < xs[i].t {
			if len(xs) == i {
				return append(xs, x)
			}
			xs = append(xs[:i+1], xs[i:]...)
			xs[i] = x
			return xs
		}
	}
	return append(xs, x)
}

// Helper struct to store precomputed values
// used in ray intersections with objects in the world
type comps struct {
	t      float64
	object *object

	point      *tuple
	underPoint *tuple
	overPoint  *tuple

	eyeV     *tuple
	normalV  *tuple
	reflectV *tuple

	inside bool

	n1 float64
	n2 float64
}

func preComputeIntersection(x *intersection, r *ray, xs []*intersection) *comps {
	n1 := 0.0
	n2 := 0.0
	// Refraction
	containers := []*intersection{}
	for _, currentX := range xs {
		if currentX.equals(x) {
			if len(containers) == 0 {
				n1 = 1.0
			} else {
				n1 = containers[len(containers)-1].o.material.refractiveIndex
			}
		}
		index, contains := intersectionsContainObject(containers, currentX.o)
		if contains {
			containers = intersectionsRemove(containers, index)
		} else {
			containers = append(containers, currentX)
		}
		if currentX.equals(x) {
			if len(containers) == 0 {
				n2 = 1.0
			} else {
				n2 = containers[len(containers)-1].o.material.refractiveIndex
			}
			break
		}
	}
	p := r.position(x.t)
	normalV := x.o.normalAt(p, x)
	ret := &comps{
		t:       x.t,
		object:  x.o,
		point:   p,
		eyeV:    r.direction.negate(),
		normalV: normalV,
		inside:  false,
		n1:      n1,
		n2:      n2,
	}
	if ret.normalV.dot(ret.eyeV) < 0 {
		ret.inside = true
		ret.normalV = ret.normalV.negate()
	}
	ret.overPoint = p.add(ret.normalV.mul(EPSILON))
	ret.underPoint = p.sub(ret.normalV.mul(EPSILON))
	ret.reflectV = r.direction.reflect(ret.normalV)
	return ret
}

func (w *world) shadeHit(c *comps, depthRemaining int) *color {
	surfaceColors := []*color{}
	for _, l := range w.lights {
		surfaceColors = append(surfaceColors,
			lighting(c.object, l, c.point, c.eyeV, c.normalV,
				w.isShadowed(c.overPoint, l)))
	}
	surface := &color{}
	for _, c := range surfaceColors {
		surface.r += c.r
		surface.g += c.g
		surface.b += c.b
	}
	surface.mul(1.0 / float64(len(surfaceColors)))

	reflected := w.reflectedColor(c, depthRemaining)
	refracted := w.refractedColor(c, depthRemaining)

	m := c.object.material
	if m.reflective > 0.0 && m.transparency > 0.0 {
		reflectance := schlick(c)
		return surface.
			add(reflected.mul(reflectance)).
			add(refracted.mul(1.0 - reflectance))
	}

	return surface.add(reflected).add(refracted)
}

func (w *world) colorAt(r *ray, depthRemaining int) *color {
	xs := w.intersects(r)
	x := hit(xs)
	if x == nil {
		return newColor(0, 0, 0)
	}
	return w.shadeHit(preComputeIntersection(x, r, xs), depthRemaining)
}

func (w *world) isShadowed(p *tuple, l *light) bool {
	v := l.position.sub(p)
	dist := v.mag()
	dir := v.normalize()
	r := newRay(p, dir)
	// TODO: Fix this so shadows cast after non shadow casting object are counted
	h := hit(w.intersects(r))
	return h != nil && !h.o.getMaterial().noShadow && h.t < dist
}

func (w *world) reflectedColor(c *comps, depthRemaining int) *color {
	if depthRemaining <= 0 || isClose(c.object.material.reflective, 0.0) {
		return newColor(0, 0, 0)
	}
	refRay := newRay(c.overPoint.copy(), c.reflectV.copy())
	refCol := w.colorAt(refRay, depthRemaining-1)
	return refCol.mul(c.object.material.reflective)
}

func (w *world) refractedColor(c *comps, depthRemaining int) *color {
	if depthRemaining <= 0 || c.object.material.transparency == 0.0 {
		return newColor(0, 0, 0)
	}

	// Snells sin(theta)/cos(theta) = n2/n1
	nRatio := c.n1 / c.n2
	cosI := c.eyeV.dot(c.normalV)
	sin2Theta := nRatio * nRatio * (1 - cosI*cosI)
	// Total internal refraction
	if sin2Theta > 1.0 {
		return newColor(0, 0, 0)
	}
	cosTheta := math.Sqrt(1.0 - sin2Theta)

	dir := c.normalV.mul(nRatio*cosI - cosTheta).sub(c.eyeV.mul(nRatio))
	refractedRay := newRay(c.underPoint, dir)

	return w.colorAt(refractedRay, depthRemaining-1).
		mul(c.object.material.transparency)
}

func schlick(c *comps) float64 {
	cos := c.eyeV.dot(c.normalV)
	if c.n1 > c.n2 {
		n := c.n1 / c.n2
		sin2Theta := n * n * (1.0 - cos*cos)
		if sin2Theta > 1.0 {
			return 1.0
		}
		cosTheta := math.Sqrt(1.0 - sin2Theta)
		cos = cosTheta
	}
	r0 := math.Pow((c.n1-c.n2)/(c.n1+c.n2), 2)
	return r0 + (1.0-r0)*math.Pow(1-cos, 5)
}
