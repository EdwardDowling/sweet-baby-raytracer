package lib

import (
	"math"
	"testing"
)

func TestDefaultWorld(t *testing.T) {
	s1 := newSphere()
	s1.material.color = newColor(0.8, 1.0, 0.6)
	s1.material.diffuse = 0.7
	s1.material.specular = 0.2

	s2 := newSphere()
	s2.setTransform(scaling(0.5, 0.5, 0.5))
	l := newPointLight(newPoint(-10, 10, -10), newColor(1, 1, 1))
	w := defaultWorld()

	if !w.lights[0].equals(l) {
		t.Error("incorrect default world light")
	}

	if !w.contains(s1) || !w.contains(s2) {
		t.Error("incorrect default world objects")
	}
}

func TestIntersects(t *testing.T) {
	w := defaultWorld()
	r := newRay(newPoint(0, 0, -5), newVector(0, 0, 1))
	xs := w.intersects(r)
	if len(xs) != 4 {
		t.Error("incorrect result for world intersect")
	}

	if !isClose(xs[0].t, 4.0) {
		t.Error("incorrect result for world intersect")
	}
	if !isClose(xs[1].t, 4.5) {
		t.Error("incorrect result for world intersect")
	}
	if !isClose(xs[2].t, 5.5) {
		t.Error("incorrect result for world intersect")
	}
	if !isClose(xs[3].t, 6.0) {
		t.Error("incorrect result for world intersect")
	}
}

func TestPreComputeIntersection(t *testing.T) {
	r := newRay(newPoint(0, 0, 0), newVector(0, 0, 1))
	s := newSphere()
	i := newIntersection(1.0, s)
	comps := preComputeIntersection(i, r, []*intersection{})

	if !comps.point.equals(newPoint(0, 0, 1)) {
		t.Error("incorrect result from pre computation of intersection")
	}
	if !comps.eyeV.equals(newVector(0, 0, -1)) {
		t.Error("incorrect result from pre computation of intersection")
	}
	if !comps.normalV.equals(newVector(0, 0, -1)) {
		t.Error("incorrect result from pre computation of intersection")
	}
	if !comps.inside {
		t.Error("incorrect result from pre computation of intersection")
	}

	r = newRay(newPoint(0, 0, -5), newVector(0, 0, 1))
	s = newSphere()
	s.transform.translate(0, 0, 1)
	i = newIntersection(5, s)
	comps = preComputeIntersection(i, r, []*intersection{})
	if comps.overPoint.z >= -EPSILON/2.0 {
		t.Error("incorrect result from pre computation of intersection overPoint")
	}
	if comps.point.z <= comps.overPoint.z {
		t.Error("incorrect result from pre computation of intersection overPoint")
	}
}

func TestColorAt(t *testing.T) {
	w := defaultWorld()
	r := newRay(newPoint(0, 0, -5), newVector(0, 1, 0))
	if !newColor(0, 0, 0).equals(w.colorAt(r, MAX_RECURSION_DEPTH)) {
		t.Error("incorrect result for world colorAt")
	}

	r = newRay(newPoint(0, 0, -5), newVector(0, 0, 1))
	if !newColor(0.380661193081, 0.475826491351, 0.285495894810).
		equals(w.colorAt(r, MAX_RECURSION_DEPTH)) {
		t.Error("incorrect result for world colorAt")
	}

	for _, o := range w.objects {
		o.getMaterial().ambient = 1.0
	}
	r = newRay(newPoint(0, 0, 0.75), newVector(0, 0, -1))
	innerColor := w.objects[1].getMaterial().color
	if !innerColor.equals(w.colorAt(r, MAX_RECURSION_DEPTH)) {
		t.Error("incorrect result for world colorAt")
	}
}

func TestShadeHit(t *testing.T) {
	w := defaultWorld()
	r := newRay(newPoint(0, 0, -5), newVector(0, 0, 1))
	s := w.objects[0]
	i := newIntersection(4.0, s)
	comps := preComputeIntersection(i, r, []*intersection{})
	c := w.shadeHit(comps, MAX_RECURSION_DEPTH)
	if !c.equals(newColor(0.38066119308103435,
		0.47582649135129296, 0.28549589481077575)) {
		t.Error("incorrect result for shade hit")
	}

	w = defaultWorld()
	w.lights = []*light{newPointLight(newPoint(0, 0.25, 0), newColor(1, 1, 1))}
	r = newRay(newPoint(0, 0, 0), newVector(0, 0, 1))
	s = w.objects[1]
	i = newIntersection(0.5, s)
	comps = preComputeIntersection(i, r, []*intersection{})
	c = w.shadeHit(comps, MAX_RECURSION_DEPTH)
	if !c.equals(newColor(0.9049844720832575,
		0.9049844720832575, 0.9049844720832575)) {
		t.Error("incorrect result for shade hit")
		t.Error(c.r, c.g, c.b)
	}
}

func glassSphere(t *testing.T) *object {
	t.Helper()
	s := newSphere()
	s.material.transparency = 1.0
	s.material.refractiveIndex = 1.5
	return s
}

func TestProComputeRefraction(t *testing.T) {
	A := glassSphere(t)
	A.transform = A.transform.scale(2, 2, 2)
	A.material.refractiveIndex = 1.5

	B := glassSphere(t)
	B.transform = B.transform.translate(0, 0, -0.25)
	B.material.refractiveIndex = 2.0

	C := glassSphere(t)
	C.transform = C.transform.translate(0, 0, 0.25)
	C.material.refractiveIndex = 2.5

	r := newRay(newPoint(0, 0, -4), newVector(0, 0, 1))
	xs := []*intersection{
		newIntersection(2, A),
		newIntersection(2.75, B),
		newIntersection(3.25, C),
		newIntersection(4.75, B),
		newIntersection(5.25, C),
		newIntersection(6, A),
	}
	for index, tc := range []struct {
		n1 float64
		n2 float64
	}{
		{
			n1: 1.0, n2: 1.5,
		},
		{
			n1: 1.5, n2: 2.0,
		},
		{
			n1: 2.0, n2: 2.5,
		},
		{
			n1: 2.5, n2: 2.5,
		},
		{
			n1: 2.5, n2: 1.5,
		},
		{
			n1: 1.5, n2: 1.0,
		},
	} {
		comps := preComputeIntersection(xs[index], r, xs)
		if !isClose(tc.n1, comps.n1) || !isClose(tc.n2, comps.n2) {
			t.Errorf("incorrect n1 n2 for tc: %d", index)
			t.Errorf("got: %f %f", comps.n1, comps.n2)
			t.Errorf("want: %f %f", tc.n1, tc.n2)
		}
	}
}

func TestRefractedColorOpaque(t *testing.T) {
	w := defaultWorld()
	s := w.objects[0]
	r := newRay(newPoint(0, 0, -5), newVector(0, 0, 1))
	xs := []*intersection{
		newIntersection(4, s),
		newIntersection(6, s),
	}
	comps := preComputeIntersection(xs[0], r, xs)
	c := w.refractedColor(comps, 5)
	if !c.equals(newColor(0, 0, 0)) {
		t.Error("incorrect refracted color")
	}
}

func TestRefractedColorMaxRecursion(t *testing.T) {
	w := defaultWorld()
	s := w.objects[0]
	s.material.transparency = 1.0
	s.material.refractiveIndex = 1.5
	r := newRay(newPoint(0, 0, -5), newVector(0, 0, 1))
	xs := []*intersection{
		newIntersection(4, s),
		newIntersection(6, s),
	}
	comps := preComputeIntersection(xs[0], r, xs)
	c := w.refractedColor(comps, 0)
	if !c.equals(newColor(0, 0, 0)) {
		t.Error("incorrect refracted color")
	}
}

func TestRefractedColorInternalReflection(t *testing.T) {
	w := defaultWorld()
	s := w.objects[0]
	s.material.transparency = 1.0
	s.material.refractiveIndex = 1.5
	r2 := math.Sqrt(2) / 2.0
	r := newRay(newPoint(0, 0, r2), newVector(0, 1, 0))
	xs := []*intersection{
		newIntersection(-r2, s),
		newIntersection(r2, s),
	}
	comps := preComputeIntersection(xs[1], r, xs)
	c := w.refractedColor(comps, 5)
	if !c.equals(newColor(0, 0, 0)) {
		t.Error("incorrect refracted color")
	}
}

func TestRefractedColorRefractedRay(t *testing.T) {
	w := defaultWorld()
	A := w.objects[0]
	A.material.ambient = 1.0
	A.material.pattern = newTestPattern()

	B := w.objects[1]
	B.material.transparency = 1.0
	B.material.refractiveIndex = 1.5

	r := newRay(newPoint(0, 0, 0.1), newVector(0, 1, 0))
	xs := []*intersection{
		newIntersection(-0.9899, A),
		newIntersection(-0.4899, B),
		newIntersection(0.4899, B),
		newIntersection(0.9899, A),
	}
	comps := preComputeIntersection(xs[2], r, xs)
	c := w.refractedColor(comps, 5)
	if !c.equals(newColor(0, 0.9988846813665367, 0.04721645191320928)) {
		t.Error("incorrect refracted color")
		t.Error(c.r, c.g, c.b)
	}
}

func TestTransparentShadeHit(t *testing.T) {
	w := defaultWorld()

	floor := newPlane()
	floor.setTransform(floor.transform.translate(0, -1, 0))
	floor.material.transparency = 0.5
	floor.material.refractiveIndex = 1.5
	w.objects = append(w.objects, floor)

	ball := newSphere()
	ball.material.color = newColor(1, 0, 0)
	ball.material.ambient = 0.5
	ball.setTransform(ball.transform.translate(0, -3.5, -0.5))
	w.objects = append(w.objects, ball)

	r2 := math.Sqrt(2) / 2.0
	r := newRay(newPoint(0, 0, -3), newVector(0, -r2, r2))
	xs := []*intersection{newIntersection(math.Sqrt(2), floor)}
	comps := preComputeIntersection(xs[0], r, xs)
	c := w.shadeHit(comps, 5)
	if !c.equals(newColor(0.9364253889815014,
		0.6864253889815014, 0.6864253889815014)) {
		t.Error("incorrect refracted color")
		t.Error(c.r, c.g, c.b)
	}
}

func TestSchlick(t *testing.T) {
	s := glassSphere(t)
	r := newRay(newPoint(0, 0.99, -2), newVector(0, 0, 1))
	xs := []*intersection{newIntersection(1.8589, s)}
	comps := preComputeIntersection(xs[0], r, xs)
	reflectance := schlick(comps)
	if !isClose(reflectance, 0.4887308101221217) {
		t.Error("incorrect reflectance")
	}
	r2 := math.Sqrt(2) / 2
	r = newRay(newPoint(0, 0, r2), newVector(0, 1, 0))
	xs = []*intersection{newIntersection(-r2, s), newIntersection(r2, s)}
	comps = preComputeIntersection(xs[1], r, xs)
	reflectance = schlick(comps)
	if !isClose(reflectance, 1.0) {
		t.Error("incorrect reflectance")
	}
}
