package lib

import "math"

type light struct {
	intensity *color
	position  *tuple
}

func newPointLight(position *tuple, intensity *color) *light {
	return &light{
		intensity: intensity,
		position:  position,
	}
}

func lighting(ob *object, light *light,
	point *tuple, eyeV, normalV *tuple, shadowed bool) *color {
	material := ob.getMaterial()
	baseColor := material.color
	if material.pattern != nil {
		obPoint := ob.worldToObject(point)
		baseColor = material.pattern.colorAt(obPoint)
	}

	effectiveColor := light.intensity.hamardProduct(baseColor)
	// Dir to light source
	lightV := light.position.sub(point).normalize()
	ambient := effectiveColor.mul(material.ambient)
	if shadowed {
		return ambient
	}
	var diffuse *color
	var specular *color
	// Cosine of angle between lightV and normal
	// negatives are on other side of surface
	lightDotNormal := lightV.dot(normalV)
	if lightDotNormal < 0 {
		diffuse = newColor(0, 0, 0)
		specular = newColor(0, 0, 0)
	} else {
		diffuse = effectiveColor.mul(material.diffuse).mul(lightDotNormal)
		// Cosine of angle between reflection vector and eye vector
		// negative means light reflects away
		reflectV := lightV.negate().reflect(normalV)
		reflectDotEye := reflectV.dot(eyeV)
		if reflectDotEye <= 0 {
			specular = newColor(0, 0, 0)
		} else {
			factor := math.Pow(reflectDotEye, material.shininess)
			specular = light.intensity.mul(material.specular).mul(factor)
		}
	}
	return ambient.add(diffuse).add(specular)
}

func (l *light) equals(o *light) bool {
	return l.intensity.equals(o.intensity) &&
		l.position.equals(o.position)
}
