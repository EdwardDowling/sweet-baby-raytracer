package lib

import "testing"

func TestAddingChild(t *testing.T) {
	g := newGroup()
	s := newSphere()
	g.addObject(s)
	if group, ok := g.bo.(*group); ok {
		if len(group.objects) != 1 {
			t.Error("incorrect number of child objects")
		}
		if s.parent != g {
			t.Error("incorrect parent")
		}
		return
	}
	t.Error("group is not group")
}
