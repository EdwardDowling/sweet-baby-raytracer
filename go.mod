module github.com/EdwardDowling/ray-tracer

go 1.21.5

require (
	github.com/aquilax/go-perlin v1.1.0 // indirect
	golang.org/x/text v0.14.0 // indirect
)
